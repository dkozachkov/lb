module.exports = {
  root: true,
  env: {
    node: true,
    es6: true
  },
  parserOptions: {
    ecmaVersion: 8,
    parser: 'babel-eslint',
    // ecmaVersion: 2017,
    sourceType: 'module'
  },
  extends: ['plugin:vue/recommended', 'eslint:recommended'],
  rules: {
    'vue/script-indent': ['error', 2, { baseIndent: 0 }],
    'no-debugger': 0,
    'no-console': 0,
    // semi: 0,
    quotes: ['error', 'single'],
    'no-prototype-builtins': 0
  },
  plugins: ['vue']
};
