<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * Class Chatbot
 *
 * @package App\Models\Chatbot
 * @property integer $id
 * @property integer $user_id
 * @property string $uuid
 * @property string $title
 * @property string $sub_title
 * @property string $avatar
 * @property string $status
 * @property string $favicon
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chatbot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chatbot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chatbot query()
 * @mixin \Eloquent
 * @property-read \App\Models\ChatbotDesign $design
 * @property-read \App\Models\User $user
 */
	final class Chatbot extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ChatbotDesign
 *
 * @property-read \App\Models\Chatbot $chatbot
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotDesign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotDesign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotDesign query()
 */
	final class ChatbotDesign extends \Eloquent {}
}

namespace App\Models{
/**
 * Class User
 *
 * @package App\Models\User
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $provider_facebook
 * @property string $provider_google
 * @property string $provider_vkontakte
 * @property string $email_verified_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chatbot[] $chatbots
 */
	final class User extends \Eloquent {}
}

