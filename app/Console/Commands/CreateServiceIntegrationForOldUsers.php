<?php

namespace App\Console\Commands;

use App\Models\Chatbot;
use App\Models\User;
use Illuminate\Console\Command;

class CreateServiceIntegrationForOldUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service:integration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The command is to create service integration functionality for old users who not have service integration functionality.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * @var User[] $oldUsers
         */
        $oldUsers = User::whereDoesntHave('serviceIntegrations')->get();
        $userNeedServiceIntegrations = false;

        if ($oldUsers->count() > 0) {
            foreach ($oldUsers as $oldUser) {
                \DB::transaction(function () use ($oldUser, &$userNeedServiceIntegrations) {
                    /**
                     * @var \App\Models\ServiceIntegration $serviceIntegration
                     */
                    $serviceIntegration = $oldUser->serviceIntegrations()->create([
                        'email_theme' => config('service_integration.email_theme'),
                    ]);

                    $serviceIntegration->emails()->create([
                        'email' => $oldUser->email,
                    ]);

                    $userNeedServiceIntegrations = true;
                }, 3);
            }
            $this->info('Service integrations for old users are created.');
        }

        /**
         * @var Chatbot[] $oldChatbots
         */
        $oldChatbots = Chatbot::whereDoesntHave('serviceIntegrations')->get();
        $chatbotsNeedServiceIntegrations = false;

        if ($oldChatbots->count() > 0) {
            foreach ($oldChatbots as $oldChatbot) {
                \DB::transaction(function () use ($oldChatbot, &$chatbotsNeedServiceIntegrations) {
                    /**
                     * @var \App\Models\ServiceIntegration $serviceIntegration
                     */
                    $serviceIntegration = $oldChatbot->serviceIntegrations()->create([
                        'email_theme' => config('service_integration.email_theme'),
                    ]);

                    /**
                     * @var User $user
                     */
                    $user = $oldChatbot->user()->firstOrFail();

                    /**
                     * @var \App\Models\ServiceIntegration $userServiceIntegration
                     */
                    $userServiceIntegration = $user->serviceIntegrations()->firstOrFail();
                    $userServiceIntegrationEmails = $userServiceIntegration->emails()
                        ->get('email')
                        ->pluck('email')
                        ->toArray();

                    foreach ($userServiceIntegrationEmails as $userServiceIntegrationEmail) {
                        $serviceIntegration->emails()->create([
                            'email' => $userServiceIntegrationEmail,
                        ]);
                    }

                    $chatbotsNeedServiceIntegrations = true;
                }, 3);
            }
            $this->info('Service integrations for old chatbots are created.');
        }

        if (!$userNeedServiceIntegrations && !$chatbotsNeedServiceIntegrations) {
            $this->info('Nothing migrate.');
        } else {
            $this->info('Done.');
        }
    }
}
