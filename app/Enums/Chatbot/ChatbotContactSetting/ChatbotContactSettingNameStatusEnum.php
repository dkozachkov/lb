<?php

declare(strict_types=1);

namespace App\Enums\Chatbot\ChatbotContactSetting;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotContactSettingNameStatusEnum
 *
 * @package App\Enums\Chatbot\ChatbotContactSetting
 *
 * @method static ChatbotContactSettingNameStatusEnum DEACTIVATED()
 * @method static ChatbotContactSettingNameStatusEnum ACTIVATED()
 */
final class ChatbotContactSettingNameStatusEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
