<?php

declare(strict_types=1);

namespace App\Enums\Chatbot\ChatbotContactSetting;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotContactSettingNeedEmailEnum
 *
 * @package App\Enums\Chatbot\ChatbotContactSetting
 *
 * @method static ChatbotContactSettingNeedEmailEnum DEACTIVATED()
 * @method static ChatbotContactSettingNeedEmailEnum ACTIVATED()
 */
final class ChatbotContactSettingNeedEmailEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
