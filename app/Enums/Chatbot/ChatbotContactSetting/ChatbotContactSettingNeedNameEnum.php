<?php

declare(strict_types=1);

namespace App\Enums\Chatbot\ChatbotContactSetting;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotContactSettingNeedNameEnum
 *
 * @package App\Enums\Chatbot\ChatbotContactSetting
 *
 * @method static ChatbotContactSettingNeedNameEnum DEACTIVATED()
 * @method static ChatbotContactSettingNeedNameEnum ACTIVATED()
 */
final class ChatbotContactSettingNeedNameEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
