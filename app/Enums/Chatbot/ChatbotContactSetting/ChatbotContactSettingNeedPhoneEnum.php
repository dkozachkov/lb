<?php

declare(strict_types=1);

namespace App\Enums\Chatbot\ChatbotContactSetting;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotContactSettingNeedPhoneEnum
 *
 * @package App\Enums\Chatbot\ChatbotContactSetting
 *
 * @method static ChatbotContactSettingNeedPhoneEnum DEACTIVATED()
 * @method static ChatbotContactSettingNeedPhoneEnum ACTIVATED()
 */
final class ChatbotContactSettingNeedPhoneEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
