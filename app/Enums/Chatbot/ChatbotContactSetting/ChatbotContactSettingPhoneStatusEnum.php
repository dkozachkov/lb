<?php

declare(strict_types=1);

namespace App\Enums\Chatbot\ChatbotContactSetting;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotContactSettingPhoneStatusEnum
 *
 * @package App\Enums\Chatbot\ChatbotContactSetting
 *
 * @method static ChatbotContactSettingPhoneStatusEnum DEACTIVATED()
 * @method static ChatbotContactSettingPhoneStatusEnum ACTIVATED()
 */
final class ChatbotContactSettingPhoneStatusEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
