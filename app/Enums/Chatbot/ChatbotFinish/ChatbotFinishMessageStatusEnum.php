<?php

declare(strict_types=1);

namespace App\Enums\Chatbot\ChatbotFinish;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotFinishMessageStatusEnum
 *
 * @package App\Enums\Chatbot\ChatbotFinish
 *
 * @method static ChatbotFinishMessageStatusEnum DEACTIVATED()
 * @method static ChatbotFinishMessageStatusEnum ACTIVATED()
 */
final class ChatbotFinishMessageStatusEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
