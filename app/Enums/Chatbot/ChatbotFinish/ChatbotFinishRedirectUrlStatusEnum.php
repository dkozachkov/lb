<?php

declare(strict_types=1);

namespace App\Enums\Chatbot\ChatbotFinish;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotFinishRedirectUrlStatusEnum
 *
 * @package App\Enums\Chatbot\ChatbotFinish
 *
 * @method static ChatbotFinishRedirectUrlStatusEnum DEACTIVATED()
 * @method static ChatbotFinishRedirectUrlStatusEnum ACTIVATED()
 */
final class ChatbotFinishRedirectUrlStatusEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
