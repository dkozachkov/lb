<?php

declare(strict_types=1);

namespace App\Enums\Chatbot;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotHasPageEnum
 *
 * @package App\Enums\Chatbot
 *
 * @method static ChatbotHasPageEnum DOESNT_HAS()
 * @method static ChatbotHasPageEnum HAS()
 */
final class ChatbotHasPageEnum extends Enum
{
    const __default = self::DOESNT_HAS;

    const DOESNT_HAS = 1;
    const HAS = 2;
}
