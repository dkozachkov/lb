<?php

declare(strict_types=1);

namespace App\Enums\Chatbot;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotIntegrationEmilStatusEnum
 *
 * @package App\Enums\Chatbot
 *
 * @method static ChatbotIntegrationEmailStatusEnum DEACTIVATED()
 * @method static ChatbotIntegrationEmailStatusEnum ACTIVATED()
 */
final class ChatbotIntegrationEmailStatusEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
