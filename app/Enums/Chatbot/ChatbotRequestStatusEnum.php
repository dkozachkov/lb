<?php

declare(strict_types=1);

namespace App\Enums\Chatbot;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotRequestStatusEnum
 *
 * @package App\Enums\Chatbot
 *
 * @method static ChatbotRequestStatusEnum DEACTIVATED()
 * @method static ChatbotRequestStatusEnum ACTIVATED()
 */
final class ChatbotRequestStatusEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
