<?php

declare(strict_types=1);

namespace App\Enums\Chatbot;


use MadWeb\Enum\Enum;

/**
 * Class ChatbotStatusEnum
 *
 * @package App\Enums\Chatbot
 *
 * @method static ChatbotStatusEnum DEACTIVATED()
 * @method static ChatbotStatusEnum ACTIVATED()
 */
final class ChatbotStatusEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
