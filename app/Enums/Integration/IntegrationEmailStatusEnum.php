<?php

declare(strict_types=1);

namespace App\Enums\Integration;


use MadWeb\Enum\Enum;

/**
 * Class IntegrationEmilStatusEnum
 *
 * @package App\Enums\Integration
 *
 * @method static IntegrationEmailStatusEnum DEACTIVATED()
 * @method static IntegrationEmailStatusEnum ACTIVATED()
 */
final class IntegrationEmailStatusEnum extends Enum
{
    const __default = self::ACTIVATED;

    const DEACTIVATED = 1;
    const ACTIVATED = 2;
}
