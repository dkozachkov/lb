<?php

namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

/**
 * Class RequestExport
 *
 * @package App\Exports
 */
final class RequestExport implements FromView, WithCustomValueBinder
{

    private $requests;

    /**
     * RequestExport constructor.
     *
     * @param $requests
     */
    public function __construct($requests)
    {
        $this->requests = $requests;
    }

    /**
     * @param Cell $cell
     * @param mixed $value
     *
     * @return bool
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, DataType::TYPE_STRING);

        return true;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('exports.requests', [
            'requests' => $this->requests,
        ]);
    }

}
