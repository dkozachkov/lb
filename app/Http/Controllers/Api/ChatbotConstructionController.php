<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Construction\UpdateRequest;
use App\Http\Resources\ChatbotConstructionResource;
use App\Services\ChatbotConstruction\ChatbotConstructionServiceContract;

/**
 * Class ChatbotConstructionController
 *
 * @package App\Http\Controllers\Api
 */
final class ChatbotConstructionController extends Controller
{

    /**
     * @var \App\Services\ChatbotConstruction\ChatbotConstructionService $service
     */
    private $service;

    /**
     * ChatbotConstructionController constructor.
     *
     * @param ChatbotConstructionServiceContract $service
     */
    public function __construct(ChatbotConstructionServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $chatbotId
     *
     * @return \App\Models\Chatbot
     */
    public function get(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->get($chatbotId);

        return $chatbot;
    }

    /**
     * @param UpdateRequest $request
     * @param int $chatbotId
     *
     * @return ChatbotConstructionResource
     */
    public function update(UpdateRequest $request, int $chatbotId)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\ChatbotConstruction $construction
         */
        $construction = $this->service->update($chatbotId, $params);

        return new ChatbotConstructionResource($construction);
    }

}
