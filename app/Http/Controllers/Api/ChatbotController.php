<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Chatbot\{ChangeTitleRequest, IndexRequest, StoreRequest, UpdateRequest};
use App\Http\Resources\ChatbotResource;
use App\Services\Chatbot\ChatbotServiceContract;

/**
 * Class ChatbotController
 *
 * @package App\Http\Controllers\Api
 */
final class ChatbotController extends Controller
{

    /**
     * @var ChatbotServiceContract $service
     */
    private $service;

    /**
     * ChatbotController constructor.
     *
     * @param ChatbotServiceContract $service
     */
    public function __construct(ChatbotServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(IndexRequest $request)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        $this->service->setUser(\Auth::user());

        /**
         * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $chatbots
         */
        $chatbots = $this->service->getChatbots($params);

        return $chatbots;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     *
     * @return ChatbotResource
     */
    public function store(StoreRequest $request)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->store($params);

        return new ChatbotResource($chatbot);
    }

    /**
     * Display the specified resource.
     *
     * @param int $chatbotId
     *
     * @return ChatbotResource
     */
    public function show(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->getChatbot($chatbotId);

        return new ChatbotResource($chatbot);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $chatbotId
     *
     * @return ChatbotResource
     */
    public function update(UpdateRequest $request, int $chatbotId)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->update($chatbotId, $params);

        return new ChatbotResource($chatbot);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $chatbotId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());
        $this->service->delete($chatbotId);

        return response()->json([], 202);
    }

    /**
     * @param int $chatbotId
     *
     * @return ChatbotResource
     */
    public function duplicate(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->duplicate($chatbotId);

        return new ChatbotResource($chatbot);
    }

    /**
     * @param int $chatbotId
     *
     * @return ChatbotResource
     */
    public function toggleStatus(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->toggleStatus($chatbotId);

        return new ChatbotResource($chatbot);
    }

    /**
     * @param ChangeTitleRequest $request
     * @param int $chatbotId
     *
     * @return ChatbotResource
     */
    public function changeTitle(ChangeTitleRequest $request, int $chatbotId)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->changeTitle($chatbotId, $params);

        return new ChatbotResource($chatbot);
    }
}
