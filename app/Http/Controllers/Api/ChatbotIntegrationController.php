<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ChatbotIntegrationResource;
use App\Http\Requests\Api\Integration\UpdateRequest;
use App\Services\ChatbotIntegration\ChatbotIntegrationServiceContract;

final class ChatbotIntegrationController extends Controller
{

    /**
     * @var ChatbotIntegrationServiceContract $service
     */
    private $service;

    /**
     * ChatbotIntegrationController constructor.
     *
     * @param ChatbotIntegrationServiceContract $service
     */
    public function __construct(ChatbotIntegrationServiceContract $service)
    {
        $this->service = $service;
    }

    public function show(int $chatbotId)
    {
        /**
         * @var \App\Models\ChatbotIntegration $integration
         */
        $integration = $this->service->show($chatbotId);

        return (new ChatbotIntegrationResource($integration))
            ->response()
            ->withHeaders([
                'Cache-Control' => 'no-cache, no-store, must-revalidate',
                'Pragma' => 'no-cache',
                'Expires' => '0',
            ]);
    }

    /**
     * @param UpdateRequest $request
     * @param int $chatbotId
     *
     * @return ChatbotIntegrationResource
     */
    public function update(UpdateRequest $request, int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var array $params
         */
        $params = $request->validated();

        if (isset($params['domain'])) {
            $domain = get_domain_from_url($params['domain']);
            unset($params['domain']);
        } else {
            $domain = '';
        }

        prepare_param($params, 'button_color');
        prepare_param($params, 'text_color');
        prepare_param($params, 'button_text');

        /**
         * @var \App\Models\ChatbotIntegration $integration
         */
        $integration = $this->service->update($chatbotId, $params, $domain);

        return new ChatbotIntegrationResource($integration);
    }
}
