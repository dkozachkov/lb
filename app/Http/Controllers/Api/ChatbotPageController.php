<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Page\{StoreRequest, UpdateRequest};
use App\Http\Resources\ChatbotPageResource;
use App\Models\ChatbotPage;
use App\Services\ChatbotPage\ChatbotPageServiceContract;

/**
 * Class ChatbotPageController
 *
 * @package App\Http\Controllers
 */
final class ChatbotPageController extends Controller
{

    /**
     * @var ChatbotPageServiceContract $service
     */
    private $service;

    /**
     * ChatbotPageController constructor.
     *
     * @param ChatbotPageServiceContract $service
     */
    public function __construct(ChatbotPageServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $chatbotId)
    {
        /**
         * @var ChatbotPage $page
         */
        $page = $this->service->show($chatbotId);

        return (new ChatbotPageResource($page))
            ->response()
            ->withHeaders([
                'Cache-Control' => 'no-cache, no-store, must-revalidate',
                'Pragma' => 'no-cache',
                'Expires' => '0',
            ]);
    }

    /**
     * @param StoreRequest $request
     * @param int $chatbotId
     * @return ChatbotPageResource
     */
    public function store(StoreRequest $request, int $chatbotId)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        prepare_params($params);

        /**
         * @var array $benefits
         */
        $benefits = prepare_array_param($params, 'benefits');

        /**
         * @var array $seo
         */
        $seo = prepare_array_param($params, 'seo');
        prepare_param($seo, 'description');
        prepare_param($seo, 'policy_url');

        $this->service->setUser(\Auth::user());

        /**
         * @var ChatbotPage $page
         */
        $page = $this->service->store($chatbotId, $params, $benefits, $seo);

        return new ChatbotPageResource($page);
    }

    /**
     * @param UpdateRequest $request
     * @param int $chatbotId
     *
     * @return ChatbotPageResource
     */
    public function update(UpdateRequest $request, int $chatbotId)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        prepare_params($params);

        /**
         * @var array $benefits
         */
        $benefits = prepare_array_param($params, 'benefits');

        /**
         * @var array $seo
         */
        $seo = prepare_array_param($params, 'seo');

        $this->service->setUser(\Auth::user());
        /**
         * @var ChatbotPage $page
         */
        $page = $this->service->update($chatbotId, $params, $benefits, $seo);

        return new ChatbotPageResource($page);
    }
}
