<?php

namespace App\Http\Controllers\Api;


use App\Enums\Chatbot\ChatbotRequestStatusEnum;
use App\Exports\RequestExport;
use App\Http\Controllers\Controller;
use App\Models\{Chatbot, ChatbotRequest};
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class ChatbotRequestController
 *
 * @package App\Http\Controllers\Api
 */
final class ChatbotRequestController extends Controller
{

    /**
     * @param int $chatbotId
     * @param Request $request
     *
     * @return ChatbotRequest
     */
    public function index(int $chatbotId, Request $request)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        /**
         * @var int $perPage
         */
        $perPage = (int)$request->input('count') ?? 20;

        /**
         * @var ChatbotRequest $requests
         */
        $requests = $chatbot->request()
            ->orderByDesc('status')
            ->orderByDesc('created_at')
            ->paginate($perPage);

        return $requests;
    }

    /**
     * @param int $chatbotId
     * @param int $requestId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleStatus(int $chatbotId, int $requestId)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        /**
         * @var ChatbotRequest $request
         */
        $request = $chatbot->request()->findOrFail($requestId);

        $request->update([
            'status' => ChatbotRequestStatusEnum::ACTIVATED()
        ]);

        return response()->json([], 200);
    }

    /**
     * @param int $chatbotId
     * @param int $requestId
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function delete(int $chatbotId, int $requestId)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        /**
         * @var ChatbotRequest $request
         */
        $request = $chatbot->request()->findOrFail($requestId);

        $request->delete();

        return response()->json([], 202);
    }

    /**
     * @param int $chatbotId
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(int $chatbotId)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        /**
         * @var ChatbotRequest[] $requests
         */
        $requests = $chatbot->request()->get();

        /**
         * @var string $title
         */
        $title = trans('export.title', [
            'title' => $chatbot->title
        ], 'ru');

        return Excel::download(new RequestExport($requests), $title);
    }

    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Relations\HasMany[]
     */
    private function getChatbot(int $chatbotId)
    {
        return \Auth::user()->chatbots()
            ->findOrFail($chatbotId);
    }
}
