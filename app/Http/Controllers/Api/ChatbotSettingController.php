<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Settings\UpdateRequest;
use App\Services\ChatbotSetting\ChatbotSettingServiceContract;

/**
 * Class ChatbotSettingController
 *
 * @package App\Http\Controllers\Api
 */
final class ChatbotSettingController extends Controller
{

    /**
     * @var ChatbotSettingServiceContract $service
     */
    private $service;

    /**
     * ChatbotSettingController constructor.
     *
     * @param ChatbotSettingServiceContract $service
     */
    public function __construct(ChatbotSettingServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $chatbotId
     *
     * @return \App\Models\Chatbot
     */
    public function show(int $chatbotId)
    {
        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->show($chatbotId);

        return response()->json($chatbot)
            ->withHeaders([
                'Cache-Control' => 'no-cache, no-store, must-revalidate',
                'Pragma' => 'no-cache',
                'Expires' => '0',
            ]);
    }

    /**
     * @param UpdateRequest $request
     * @param int $chatbotId
     *
     * @return mixed
     */
    public function update(UpdateRequest $request, int $chatbotId)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        /**
         * @var array $design
         */
        $design = $params['design'];
        $this->reduxData($design);

        /**
         * @var array $contact
         */
        $contact = $params['contact_setting'];
        $this->reduxData($contact);

        /**
         * @var array $finish
         */
        $finish = $params['finish'];
        $this->reduxData($finish);

        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->update($chatbotId, $params, $design, $contact, $finish);

        return $chatbot;
    }

    /**
     * @param array $data
     */
    private function reduxData(array &$data): void
    {
        foreach ($data as $key => $_) {
            if (!isset($data[$key]) || isset($data[$key]) && empty($data[$key])) {
                if (gettype(strpos($key, '_status')) === 'integer') {
                    $data[$key] = 1;
                } else {
                    $data[$key] = '';
                }
            }
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset()
    {
        /**
         * @var array $data
         */
        $data = [
            'question_color' => config('design.question_color'),
            'answer_color' => config('design.answer_color'),
            'button_color' => config('design.button_color'),
            'button_text' => config('design.button_text'),
            'text_color' => config('design.text_color'),
            'font_question_color' => config('design.font_question_color'),
            'font_answer_color' => config('design.font_answer_color'),
            'background_color' => config('design.background_color'),
            'domain' => '',
        ];

        return response()->json($data);
    }
}
