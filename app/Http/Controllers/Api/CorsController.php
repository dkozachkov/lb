<?php

namespace App\Http\Controllers\Api;


use App\Enums\Chatbot\ChatbotIntegrationEmailStatusEnum;
use App\Enums\Integration\IntegrationEmailStatusEnum;
use App\Http\Requests\Api\Cors\SaveRequest;
use App\Mail\CreateRequestMail;
use App\Models\Chatbot;
use App\Http\Controllers\Controller;
use App\Enums\Chatbot\ChatbotStatusEnum;

/**
 * Class CorsController
 *
 * @package App\Http\Controllers\Api
 */
final class CorsController extends Controller
{

    /**
     * @param string $uuid
     *
     * @return Chatbot|\Illuminate\Http\JsonResponse
     */
    public function get(string $uuid)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = Chatbot::with([
            'design', 'construction', 'finish',
            'contactSetting', 'page',
        ])->where('uuid', $uuid)
            ->firstOrFail();

        if (empty($chatbot->construction->construction)) {
            $chatbot->construction->construction = '[]';
        }

        if (ChatbotStatusEnum::DEACTIVATED()->is((int)$chatbot->status)) {
            abort(404);
        }

        $domain = get_domain_from_url(request()->server('HTTP_REFERER', ''));
        $projectDomain = get_domain_from_url(config('app.url'));

        if (
            !empty($chatbot->domain)
            && ($domain !== $chatbot->domain && $domain !== $projectDomain)
        ) {
            return response()->json([], 403);
        }

        return $chatbot;
    }

    /**
     * @param SaveRequest $request
     * @param string $uuid
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function save(SaveRequest $request, string $uuid)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        /**
         * @var Chatbot $chatbot
         */
        $chatbot = Chatbot::where('uuid', $uuid)->firstOrFail();

        \DB::transaction(function () use ($chatbot, $params) {
            /**
             * @var \App\Models\ChatbotRequest $chatbotRequest
             */
            $chatbotRequest = $chatbot->request()->create($params);

            /**
             * @var \App\Models\ChatbotServiceIntegration $chatbotServiceIntegration
             */
            $chatbotServiceIntegration = $chatbot->serviceIntegrations()->first();

            if (ChatbotIntegrationEmailStatusEnum::ACTIVATED == $chatbotServiceIntegration->email_status) {
                $emails = $chatbotServiceIntegration->emails()
                    ->get('email')
                    ->pluck('email')
                    ->toArray();

                foreach ($emails as $email) {
                    \Mail::send(
                        new CreateRequestMail(
                            $email,
                            $chatbot->title,
                            $chatbotRequest,
                            $chatbotServiceIntegration->email_theme
                        )
                    );
                }
            } else {
                /**
                 * @var \App\Models\User $user
                 */
                $user = $chatbot->user()->firstOrFail();

                /**
                 * @var \App\Models\ServiceIntegration $serviceIntegration
                 */
                $serviceIntegration = $user->serviceIntegrations()->firstOrFail();

                if (IntegrationEmailStatusEnum::ACTIVATED == $serviceIntegration->email_status) {
                    $emails = $serviceIntegration->emails()
                        ->get('email')
                        ->pluck('email')
                        ->toArray();

                    foreach ($emails as $email) {
                        \Mail::send(
                            new CreateRequestMail(
                                $email,
                                $chatbot->title,
                                $chatbotRequest,
                                $serviceIntegration->email_theme
                            )
                        );
                    }
                }
            }


        });

        return response()->json([], 201);
    }
}
