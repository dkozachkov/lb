<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\IconResource;
use App\Models\IconCategory;

/**
 * Class IconController
 *
 * @package App\Http\Controllers\Api
 */
final class IconController extends Controller
{

    /**
     * @return IconCategory[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCategories()
    {
        $categories = IconCategory::all(['id', 'title', 'path']);

        /**
         * @var IconCategory $category
         */
        foreach ($categories as &$category) {
            $icons = \Storage::disk('public')->files($category->path);

            unset($category->path);

            if (count($icons) > 0) {
                $icon = "/storage/$icons[0]";
                $category->icon = $icon;
            }
        }

        return $categories;
    }

    public function getIcons(int $categoryId)
    {
        /**
         * @var \App\Models\IconCategory $category
         */
        $category = IconCategory::findOrFail($categoryId);

        /**
         * @var array $icons
         */
        $icons = \Storage::disk('public')->files($category->path);

        foreach ($icons as &$icon) {
            $icon = "/storage/$icon";
        }

        return IconResource::collection(collect($icons));
    }

}
