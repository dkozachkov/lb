<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Integration\ServiceIntegrationEmailServiceContract;
use App\Http\Resources\ServiceIntegrations\{EmailIntegrationsResource, ServiceIntegrationResource};
use App\Http\Requests\Api\ServiceIntegrations\EmailIntegrations\{ChangeStatusRequest, UpdateRequest};

/**
 * Class ServiceIntegrationEmailController
 *
 * @package App\Http\Controllers\Api
 */
final class ServiceIntegrationEmailController extends Controller
{
    /**
     * @var ServiceIntegrationEmailServiceContract $service
     */
    private $service;

    /**
     * ServiceIntegrationEmailController constructor.
     *
     * @param ServiceIntegrationEmailServiceContract $service
     */
    public function __construct(ServiceIntegrationEmailServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param int|null $chatbotId
     *
     * @return EmailIntegrationsResource
     */
    public function index(?int $chatbotId = null)
    {
        /**
         * @var \stdClass $result
         */
        $result = $this->service->index();

        return new EmailIntegrationsResource($result);
    }

    /**
     * @param UpdateRequest $request
     * @param int|null $chatbotId
     *
     * @return EmailIntegrationsResource
     */
    public function update(UpdateRequest $request, ?int $chatbotId = null)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        /**
         * @var string $title
         */
        $title = $params['title'];

        /**
         * @var array $emails
         */
        $emails = $params['emails'];

        /**
         * @var \stdClass $result
         */
        $result = $this->service->update($title, $emails);

        return new EmailIntegrationsResource($result);
    }

    /**
     * @param ChangeStatusRequest $request
     * @param int|null $chatbotId
     *
     * @return ServiceIntegrationResource
     */
    public function changeStatus(ChangeStatusRequest $request, ?int $chatbotId = null)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        /**
         * @var \App\Models\ServiceIntegration $serviceIntegration
         */
        $serviceIntegration = $this->service->changeStatus($params);

        return new ServiceIntegrationResource($serviceIntegration);
    }
}
