<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\AuthRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers\Auth
 */
final class AuthController extends Controller
{

    /**
     * @param AuthRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(AuthRequest $request)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        if (auth()->attempt($params)) {
            return response()->json([], 200);
        }

        return response()->json([
            'errors'  => 'errors'
        ], 422);
    }

    /**
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();
        unset($params['password_confirmation']);

        /**
         * @var array $data
         */
        $data = $params;
        $data['password'] = Hash::make($data['password']);
        unset($params['name']);

        \DB::transaction(function () use ($data) {
            /**
             * @var \App\Models\User $user
             */
            $user = User::create($data);
            /**
             * @var \App\Models\ServiceIntegration $serviceIntegrations
             */
            $serviceIntegrations = $user->serviceIntegrations()->create([
                'email_theme' => '',
            ]);

            $serviceIntegrations->emails()->create([
                'email' => $data['email']
            ]);
        }, 3);

        if (auth()->attempt($params, true)) {
            return response()->json([], 200);
        }

        return response()->json([
            'errors'  => 'errors'
        ], 422);
    }

}
