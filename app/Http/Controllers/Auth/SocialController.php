<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;


use App\Models\User;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

/**
 * Class SocialController
 *
 * @package App\Http\Controllers
 */
final class SocialController extends Controller
{
    /**
     * @var string $redirectTo
     */
    private $redirectTo = '/';

    /**
     * SocialController constructor.
     */
    public function __construct()
    {
        $this->redirectTo = route('home');
    }

    /**
     * @param $provider
     *
     * @return mixed
     */
    public function redirect(string $provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(string $provider)
    {
        $getInfo = Socialite::driver($provider)->user();
        $getInfo->email = $this->getUserEmail($getInfo, $provider);

        /**
         * @var User $user
         */
        $user = $this->createUser($getInfo, $provider);

        auth()->login($user);

        return redirect()->to($this->redirectTo);
    }

    /**
     * @param $info
     * @param string $provider
     *
     * @return string|mixed|null
     */
    private function getUserEmail($info, string $provider)
    {
        if ($provider === 'vkontakte') {
            return $info->accessTokenResponseBody['email'] ?? null;
        }

        return $info->email;
    }

    /**
     * @param $getInfo
     * @param string $provider
     *
     * @return User
     */
    private function createUser($getInfo, string $provider): User
    {
        /**
         * @var User $user
         */
        $user = User::where("email", $getInfo->email)
            ->first();

        if (!$user) {
            $user = \DB::transaction(function () use ($getInfo, $provider) {
                /**
                 * @var \App\Models\User
                 */
                $user = User::create([
                    'avatar' => $getInfo->avatar,
                    'name' => $getInfo->name,
                    'email' => $getInfo->email,
                    "provider_$provider" => $getInfo->id,
                ]);

                /**
                 * @var \App\Models\ServiceIntegration $serviceIntegrations
                 */
                $serviceIntegrations = $user->serviceIntegrations()->create([
                    'email_theme' => '',
                ]);

                $serviceIntegrations->emails()->create([
                    'email' => $user->email
                ]);

                return $user;
            }, 3);
        } else {
            $user->update([
                "provider_$provider" => $getInfo->id,
            ]);
        }

        return $user;

    }
}
