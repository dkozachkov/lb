<?php

declare(strict_types=1);

namespace App\Http\Controllers;


use App\Services\ChatbotConstruction\ChatbotConstructionServiceContract;

/**
 * Class ChatbotConstructionController
 *
 * @package App\Http\Controllers
 */
final class ChatbotConstructionController extends Controller
{
    /**
     * @var ChatbotConstructionServiceContract $service
     */
    private $service;

    /**
     * ChatbotConstructionController constructor.
     *
     * @param ChatbotConstructionServiceContract $service
     */
    public function __construct(ChatbotConstructionServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->get($chatbotId);

        return view('pages.construction')
            ->with('chatbot', $chatbot);
    }
}
