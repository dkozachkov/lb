<?php

declare(strict_types=1);

namespace App\Http\Controllers;


use App\Services\Chatbot\ChatbotServiceContract;

/**
 * Class ChatbotController
 *
 * @package App\Http\Controllers
 */
final class ChatbotController extends Controller
{

    /**
     * @var ChatbotServiceContract $service
     */
    private $service;

    /**
     * ChatbotController constructor.
     *
     * @param ChatbotServiceContract $service
     */
    public function __construct(ChatbotServiceContract $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('');
    }

    public function show(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->service->getChatbot($chatbotId);

        return view('')
            ->with('chatbot', $chatbot);
    }

}
