<?php

declare(strict_types=1);

namespace App\Http\Controllers;


use App\Models\Chatbot;
use App\Services\ChatbotIntegration\ChatbotIntegrationServiceContract;

/**
 * Class ChatbotIntegrationController
 * @package App\Http\Controllers
 */
final class ChatbotIntegrationController extends Controller
{

    /**
     * @var ChatbotIntegrationServiceContract $service
     */
    private $service;

    /**
     * ChatbotIntegrationController constructor.
     *
     * @param ChatbotIntegrationServiceContract $service
     */
    public function __construct(ChatbotIntegrationServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var array $data
         */
        $data = $this->service->showData($chatbotId);

        /**
         * @var Chatbot $chatbot
         */
        $chatbot = Chatbot::findOrFail($chatbotId);

        /**
         * @var array $buttonData
         */
        $buttonData = [
            'integration_button_text' => config('integration.integration_button_text'),
            'integration_button_color' => config('integration.integration_button_color'),
            'integration_button_text_color' => config('integration.integration_button_text_color'),
        ];

        return view('pages.integration')
            ->with('data', $data)
            ->with('chatbot', $chatbot)
            ->with('buttonData', $buttonData);
    }

}
