<?php

declare(strict_types=1);

namespace App\Http\Controllers;


use App\Services\ChatbotPage\ChatbotPageServiceContract;

/**
 * Class ChatbotPageController
 *
 * @package App\Http\Controllers
 */
final class ChatbotPageController extends Controller
{

    /**
     * @var ChatbotPageServiceContract $service
     */
    private $service;

    /**
     * ChatbotPageController constructor.
     *
     * @param ChatbotPageServiceContract $service
     */
    public function __construct(ChatbotPageServiceContract $service)
    {
        $this->service = $service;

    }

    public function create(int $chatbotId)
    {
        $chatbot = \App\Models\Chatbot::findOrFail($chatbotId);

        return view('pages.template')
               ->with('chatbot_id', $chatbotId)
               ->with('chatbot', $chatbot);
    }

    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\ChatbotPage $page
         */
        $page = $this->service->show($chatbotId);

        /**
         * @var string $uuid
         */
        $uuid = $page->chatbot->uuid;

        $chatbot = \App\Models\Chatbot::findOrFail($chatbotId);

        return view('pages.template')
            ->with('page', $page->toJson())
            ->with('uuid', $uuid)
            ->with('chatbot_id', $chatbotId)
            ->with('chatbot', $chatbot);
    }

}
