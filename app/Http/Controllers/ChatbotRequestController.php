<?php

namespace App\Http\Controllers;


use App\Enums\Chatbot\ChatbotRequestStatusEnum;
use App\Models\{Chatbot, ChatbotRequest};
use Illuminate\Http\Request;

/**
 * Class ChatbotRequestController
 *
 * @package App\Http\Controllers\Api
 */
final class ChatbotRequestController extends Controller
{

    public function index(Request $request)
    {
        /**
         * @var Chatbot[] $chatbots
         */
        $chatbots = \Auth::user()->chatbots()
            ->with([
                'contactSetting',
                'design',
                'construction',
                'finish'
            ])->get();

        return view('pages.requests', [
            'chatbots' => $chatbots,
            'chatbot_id' => $request->input('chatbot_id'),
        ]);
    }

    /**
     * @param int $chatbotId
     * @param int $requestId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $chatbotId, int $requestId)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = \Auth::user()->chatbots()
            ->findOrFail($chatbotId);

        /**
         * @var ChatbotRequest $request
         */
        $request = $chatbot->request()->findOrFail($requestId);

        $request->update(['status' => ChatbotRequestStatusEnum::ACTIVATED()]);

        return view('')
            ->with('request', $request)
            ->with('chatbot', $chatbot);
    }
}
