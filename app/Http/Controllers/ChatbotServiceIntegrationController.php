<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Chatbot;

/**
 * Class ChatbotServiceIntegrationController
 *
 * @package App\Http\Controllers
 */
final class ChatbotServiceIntegrationController extends Controller
{
    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(int $chatbotId)
    {
        /**
         * @var \App\Models\User $user
         */
        $user = \Auth::user();

        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $user->chatbots()
            ->with(['serviceIntegrations'])
            ->findOrFail($chatbotId);

        return view('pages.integrations')
            ->with('chatbot', $chatbot);
    }

}
