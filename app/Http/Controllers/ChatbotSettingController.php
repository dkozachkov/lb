<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\ChatbotSetting\ChatbotSettingServiceContract;

/**
 * Class ChatbotSettingController
 *
 * @package App\Http\Controllers
 */
final class ChatbotSettingController extends Controller
{

    /**
     * @var ChatbotSettingServiceContract $service
     */
    private $service;

    /**
     * ChatbotIntegrationController constructor.
     *
     * @param ChatbotSettingServiceContract $service
     */
    public function __construct(ChatbotSettingServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(int $chatbotId)
    {
        $this->service->setUser(\Auth::user());

        /**
         * @var \App\Models\ChatbotContactSetting $settings
         */
        $settings = $this->service->show($chatbotId);

        $chatbot = \App\Models\Chatbot::findOrFail($chatbotId);

        return view('pages.settings')
            ->with('settings', $settings)
            ->with('chatbot', $chatbot);
    }
}
