<?php

declare(strict_types=1);

namespace App\Http\Controllers;


use App\Enums\Chatbot\ChatbotStatusEnum;
use App\Models\Chatbot;

/**
 * Class PageController
 *
 * @package App\Http\Controllers
 */
final class PageController extends Controller
{

    /**
     * @param string $chatbotUuid
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(string $chatbotUuid)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = Chatbot::with([
            'design',
            'construction',
            'finish',
            'contactSetting',
            'page' => function ($query) {
                $query->with(['benefits', 'seo']);
            }
        ])->where('uuid', $chatbotUuid)
            ->firstOrFail();

        /**
         * @var bool $isDisabled
         */
        $isDisabled = ChatbotStatusEnum::DEACTIVATED == $chatbot->status;

        if ($isDisabled) {
            /**
             * @var string $title
             */
            $title = $chatbot->page->title;

            return view('pages.disabled_chatbot')
                ->with('title', $title);
        }

        return view('pages.landing-page')
            ->with('chatbot', $chatbot);
    }
}
