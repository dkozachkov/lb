<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Chatbot;

/**
 * Class ServiceIntegrationController
 *
 * @package App\Http\Controllers
 */
final class ServiceIntegrationController extends Controller
{
    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(int $chatbotId)
    {
        /**
         * @var \App\Models\User $user
         */
        $user = \Auth::user();

        /**
         * @var \App\Models\ServiceIntegration $serviceIntegrations
         */
        $serviceIntegrations = $user->serviceIntegrations()
            ->firstOrFail();

        return view('pages.integrations')
            ->with('serviceIntegrations', $serviceIntegrations);
    }

}
