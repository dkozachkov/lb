<?php

namespace App\Http\Controllers;

use App\Http\Requests\Api\ServiceIntegrations\EmailIntegrations\UpdateRequest;
use App\Http\Resources\ServiceIntegrations\EmailIntegrationsResource;
use App\Services\Integration\ServiceIntegrationEmailServiceContract;

/**
 * Class ServiceIntegrationEmailController
 *
 * @package App\Http\Controllers
 */
final class ServiceIntegrationEmailController extends Controller
{
    /**
     * @var ServiceIntegrationEmailServiceContract $service
     */
    private $service;

    /**
     * ServiceIntegrationEmailController constructor.
     *
     * @param ServiceIntegrationEmailServiceContract $service
     */
    public function __construct(ServiceIntegrationEmailServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param int|null $chatbotId
     *
     * @return EmailIntegrationsResource
     */
    public function index(?int $chatbotId = null)
    {
        /**
         * @var \stdClass $result
         */
        $result = $this->service->index();

        return new EmailIntegrationsResource($result);
    }

    /**
     * @param UpdateRequest $request
     *
     * @return EmailIntegrationsResource
     */
    public function update(UpdateRequest $request = null)
    {
        /**
         * @var array $params
         */
        $params = $request->validated();

        /**
         * @var string $title
         */
        $title = $params['title'];

        /**
         * @var array $emails
         */
        $emails = $params['emails'];

        /**
         * @var \stdClass $result
         */
        $result = $this->service->update($title, $emails);

        return new EmailIntegrationsResource($result);
    }
}
