<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\Chatbot;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ChangeTitleRequest
 *
 * @package App\Http\Requests\Api\Chatbot
 */
final class ChangeTitleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'sometimes|nullable|string|min:1|max:255',
        ];
    }
}
