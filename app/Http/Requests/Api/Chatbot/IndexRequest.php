<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\Chatbot;


use Illuminate\Foundation\Http\FormRequest;

final class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth('api')->check() ?? auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'search' => 'sometimes|nullable|string|max:255',
            'count' => 'sometimes|nullable|integer|min:1|max:50',
        ];
    }
}
