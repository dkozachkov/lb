<?php

namespace App\Http\Requests\Api\Cors;

use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingEmailStatusEnum;
use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingNameStatusEnum;
use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingPhoneStatusEnum;
use App\Models\Chatbot;
use Illuminate\Foundation\Http\FormRequest;

class SaveRequest extends FormRequest
{

    /**
     * @var Chatbot $chatbot
     */
    private $chatbot;

    public function __construct(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        $this->chatbot = Chatbot::with(['contactSetting'])
            ->where('uuid', request()->uuid)
            ->firstOrFail();

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * @var \App\Models\ChatbotContactSetting $contactSetting
         */
        $contactSetting = $this->chatbot->contactSetting;
        $rules = [
            'messages' => 'required|json'
        ];

        if (ChatbotContactSettingPhoneStatusEnum::ACTIVATED()->is((int)$contactSetting->phone_status)) {
            $rules['phone'] = 'required|string';
        }

        if (ChatbotContactSettingNameStatusEnum::ACTIVATED()->is((int)$contactSetting->name_status)) {
            $rules['name'] = 'required|string';
        }

        if (ChatbotContactSettingEmailStatusEnum::ACTIVATED()->is((int)$contactSetting->email_status)) {
            $rules['email'] = 'required|email';
        }

        return $rules;
    }
}
