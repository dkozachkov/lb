<?php

namespace App\Http\Requests\Api\Integration;

use App\Rules\DomainRule;
use Illuminate\Foundation\Http\FormRequest;

final class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth('api')->check() || auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'button_color' => 'sometimes|nullable|color',
            'text_color' => 'sometimes|nullable|color',
            'button_text' => 'sometimes|nullable|string',
            'domain' => ['nullable', 'string'],
        ];
    }
}
