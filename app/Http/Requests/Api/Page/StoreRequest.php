<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\Page;


use Illuminate\Foundation\Http\FormRequest;

final class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'nullable|image|max:10000',
            'background' => 'required|image|max:10000',
            'favicon' => 'sometimes|nullable|image|max:10000',
            'title' => 'required|string|min:1|max:255',
            'sub_title' => 'sometimes|nullable|string|max:255',
            'benefits' => 'sometimes|nullable|array',
            'benefits.*.text' => 'required|string|min:1|max:255',
            'benefits.*.image' => 'required|string',
            'seo' => 'sometimes|nullable|array',
            'seo.title' => 'sometimes|nullable|string|max:255',
            'seo.description' => 'sometimes|nullable|string',
            'seo.keywords' => 'sometimes|nullable|string|max:255',
            'seo.policy_url' => 'sometimes|nullable|url',
        ];
    }
}
