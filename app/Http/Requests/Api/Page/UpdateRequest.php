<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\Page;


use Illuminate\Foundation\Http\FormRequest;

final class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'title' => 'required|string|min:1|max:255',
            'sub_title' => 'sometimes|nullable|string|max:255',
            'benefits' => 'sometimes|nullable|array',
            'benefits.*.text' => 'sometimes|nullable|string|min:1|max:255',
            'benefits.*.image' => 'sometimes|nullable|string',
            'seo' => 'sometimes|nullable|array',
            'seo.title' => 'sometimes|nullable|string|max:255',
            'seo.description' => 'sometimes|nullable|string',
            'seo.keywords' => 'sometimes|nullable|string|max:255',
            'seo.policy_url' => 'sometimes|nullable|url',
        ];

        $this->setRule($rules, 'background');
        $this->setImageRules($rules, 'avatar');
        $this->setImageRules($rules, 'favicon');

        return $rules;
    }

    /**
     * @param array $arr
     * @param string $field
     *
     * @return void
     */
    private function setRule(array &$arr, string $field): void
    {
        if (isset($this->$field)) {
            if (is_string($this->$field)) {
                $arr[$field] = 'required|string|max:255';
            } else {
                $arr[$field] = 'required|image|max:10000';
            }
        }
    }

    /**
     * Add validation to image fields in a design
     *
     * @param array $rules
     * @param string $field
     *
     * @return void
     */
    private function setImageRules(array &$rules, string $field): void
    {
        if (isset($this->$field)) {
            if (is_string($this->$field)) {
                $rules[$field] = 'nullable|string|max:255';
            } else {
                $rules[$field] = 'nullable|image|max:10000';
            }
        }
    }
}
