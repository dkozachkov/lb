<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\ServiceIntegrations\EmailIntegrations;


use App\Enums\Integration\IntegrationEmailStatusEnum;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ChangeStatusRequest
 *
 * @package App\Http\Requests\Api\ServiceIntegrations\EmailIntagrations
 */
final class ChangeStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check() ?? auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $statuses = implode(',', IntegrationEmailStatusEnum::toArray());

        return [
            'email_status' => "required|in:$statuses",
        ];
    }

}
