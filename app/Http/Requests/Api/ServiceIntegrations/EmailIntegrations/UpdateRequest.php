<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\ServiceIntegrations\EmailIntegrations;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 *
 * @package App\Http\Requests\Api\ServiceIntegrations\EmailIntagrations
 */
final class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check() ?? auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'emails' => 'required|array',
            'emails.*' => 'required|email',
        ];
    }

}
