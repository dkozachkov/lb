<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\Settings;


use Illuminate\Foundation\Http\FormRequest;

use App\Enums\Chatbot\ChatbotFinish\{
    ChatbotFinishMessageStatusEnum,
    ChatbotFinishRedirectUrlStatusEnum
};

use App\Enums\Chatbot\ChatbotContactSetting\{
    ChatbotContactSettingEmailStatusEnum,
    ChatbotContactSettingNameStatusEnum,
    ChatbotContactSettingPhoneStatusEnum
};

/**
 * Class UpdateRequest
 *
 * @package App\Http\Requests\Api\Settings
 */
final class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check() ?? auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $phoneStatus = ChatbotContactSettingPhoneStatusEnum::ACTIVATED();
        $nameStatus = ChatbotContactSettingNameStatusEnum::ACTIVATED();
        $emailStatus = ChatbotContactSettingEmailStatusEnum::ACTIVATED();
        $messageStatus = ChatbotFinishMessageStatusEnum::ACTIVATED();
        $redirectStatus = ChatbotFinishRedirectUrlStatusEnum::ACTIVATED();

        $phoneStatusesArray = implode(',', ChatbotContactSettingPhoneStatusEnum::toArray());
        $nameStatusesArray = implode(',', ChatbotContactSettingNameStatusEnum::toArray());
        $emailStatusesArray = implode(',', ChatbotContactSettingEmailStatusEnum::toArray());

        $finishMessageStatusesArray = implode(',', ChatbotFinishMessageStatusEnum::toArray());
        $finishRedirectStatusesArray = implode(',', ChatbotFinishRedirectUrlStatusEnum::toArray());

        $rules = [
            'contact_setting' => 'nullable|array',
            'contact_setting.phone_status' => "nullable|in:$phoneStatusesArray",
            'contact_setting.phone_hints' => "nullable|required_if:contact.need_phone,$phoneStatus|string|max:255",
            'contact_setting.phone_mask' => "nullable|required_if:contact.need_phone,$phoneStatus|string|max:255",
            'contact_setting.name_status' => "nullable|in:$nameStatusesArray",
            'contact_setting.name_hints' => "nullable|required_if:contact.need_name,$nameStatus|string|max:255",
            'contact_setting.email_status' => "nullable|in:$emailStatusesArray",
            'contact_setting.email_hints' => "nullable|required_if:contact.need_email,$emailStatus|string|max:255",
            'contact_setting.message' => "nullable|string|max:255",
            'design' => 'required|array',
            'design.title' => 'required|string|max:255',
            'design.question_color' => 'nullable|color',
            'design.answer_color' => 'nullable|color',
            'design.button_color' => 'nullable|color',
            'design.font_answer_color' => 'nullable|color',
            'design.font_question_color' => 'nullable|color',
            'design.background_color' => 'nullable|color',
            'finish' => 'nullable|array',
            'finish.message_status' => "nullable|in:$finishMessageStatusesArray",
            'finish.message' => "nullable|required_if:finish.message_status,$messageStatus|string",
            'finish.sub_message' => "nullable|required_if:finish.message_status,$messageStatus|string",
            'finish.redirect_url_status' => "nullable|in:$finishRedirectStatusesArray",
            'finish.redirect_url' => "nullable|required_if:finish.redirect_url_status,$redirectStatus|url|max:255",
            'title' => 'required|string|max:255',
        ];

        $this->validateDesignImageField('background', $rules);
        $this->validateDesignImageField('avatar', $rules);

        return $rules;
    }

    /**
     * Add validation to image fields in a design
     *
     * @param string $field
     * @param array $rules
     *
     * @return void
     */
    private function validateDesignImageField(string $field, array &$rules): void
    {
        if (isset($this->design[$field])) {
            if (is_string($this->design[$field])) {
                $rules["design.$field"] = 'nullable|string|max:255';
            } else {
                $rules["design.$field"] = 'nullable|image|max:10000';
            }
        }
    }
}
