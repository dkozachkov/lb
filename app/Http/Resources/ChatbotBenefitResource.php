<?php

declare(strict_types=1);

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ChatbotBenefitResource
 *
 * @package App\Http\Resources
 */
final class ChatbotBenefitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'text' => $this->text,
            'image' => url($this->image),
        ];
    }
}
