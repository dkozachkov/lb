<?php

declare(strict_types=1);

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ChatbotConstructionResource
 *
 * @package App\Http\Resources
 */
final class ChatbotConstructionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'construction' => $this->construction,
        ];
    }
}
