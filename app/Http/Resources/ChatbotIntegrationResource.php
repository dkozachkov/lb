<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ChatbotIntegrationResource
 *
 * @package App\Http\Resources
 */
final class ChatbotIntegrationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'button_color' => $this->button_color,
            'text_color' => $this->text_color,
            'button_text' => $this->button_text,
            'domain' => $this->domain,
        ];
    }
}