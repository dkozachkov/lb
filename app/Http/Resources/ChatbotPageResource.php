<?php

declare(strict_types=1);

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ChatbotPageResource
 *
 * @package App\Http\Resources
 */
final class ChatbotPageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'sub_title' => $this->sub_title,
            'background' => url($this->background),
            'avatar' => $this->avatar ? url($this->avatar) : null,
            'favicon' => url($this->favicon),
            'benefits' => ChatbotBenefitResource::collection($this->benefits),
            'seo' => new ChatbotSeoResource($this->seo),
            'uuid' => $this->chatbot->uuid,
        ];
    }
}
