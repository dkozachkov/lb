<?php

declare(strict_types=1);

namespace App\Http\Resources;


use App\Enums\Chatbot\ChatbotHasPageEnum;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ChatbotResource
 *
 * @package App\Http\Resources
 */
final class ChatbotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'has_page' => $this->has_page ? (string) $this->has_page : ChatbotHasPageEnum::DOESNT_HAS(),
            'status' => $this->status,
        ];
    }
}
