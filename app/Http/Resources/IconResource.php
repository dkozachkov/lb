<?php

declare(strict_types=1);

namespace App\Http\Resources;


use App\Enums\Chatbot\ChatbotHasPageEnum;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class IconResource
 *
 * @package App\Http\Resources
 */
final class IconResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $title = explode('/', $this->resource);
        $title = $title[count($title) - 1];
        $title = explode('.', $title)[0];

        return [
            'path' => $this->resource,
            'title' => $title,
        ];
    }
}
