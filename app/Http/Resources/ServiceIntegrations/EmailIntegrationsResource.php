<?php

declare(strict_types=1);

namespace App\Http\Resources\ServiceIntegrations;


use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class EmailIntegrationsResource
 *
 * @package App\Http\Resources
 */
final class EmailIntegrationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [
            'title' => $this->title,
            'emails' => [],
            'email_status' => $this->email_status,
        ];

        foreach ($this->emails as $email) {
            array_push($response['emails'], $email);
        }

        return $response;
    }
}
