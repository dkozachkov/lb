<?php

declare(strict_types=1);

namespace App\Http\Resources\ServiceIntegrations;


use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ServiceIntegrationResource
 *
 * @package App\Http\Resources
 */
final class ServiceIntegrationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'email_status' => $this->email_status,
        ];
    }
}
