<?php

namespace App\Mail;

use App\Models\ChatbotRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

final class CreateRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string $email
     */
    private $email;

    /**
     * @var string $chatbotTitle
     */
    private $chatbotTitle;

    /**
     * @var string $emailTheme
     */
    private $emailTheme;

    /**
     * @var ChatbotRequest $chatbotRequest
     */
    private $chatbotRequest;

    /**
     * Create a new message instance.
     *
     * @param string $email
     * @param string $chatbotTitle
     * @param ChatbotRequest $chatbotRequest
     * @param string $emailTheme
     *
     * @return void
     */
    public function __construct(string $email, string $chatbotTitle, ChatbotRequest $chatbotRequest, string $emailTheme)
    {
        $this->email = $email;
        $this->chatbotTitle = $chatbotTitle;
        $this->chatbotRequest = $chatbotRequest;
        $this->emailTheme = $emailTheme;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->to($this->email)
            ->subject($this->emailTheme)
            ->view('emails.new_request')
            ->with('chatbotTitle', $this->chatbotTitle)
            ->with('chatbotRequest', $this->chatbotRequest)
            ->with('chatbotMessages', json_decode($this->chatbotRequest->messages));
    }
}
