<?php

declare(strict_types=1);

namespace App\Models;


use App\Enums\Chatbot\ChatbotHasPageEnum;
use App\Enums\Chatbot\ChatbotStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

/**
 * Class Chatbot
 *
 * @package App\Models\Chatbot
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $uuid
 * @property string $title
 * @property string $domain
 * @property ChatbotStatusEnum $status
 * @property ChatbotHasPageEnum $has_page
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chatbot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chatbot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chatbot query()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ChatbotBenefit[] $benefits
 * @property-read \App\Models\ChatbotConstruction $construction
 * @property-read \App\Models\ChatbotContactSetting $contactSetting
 * @property-read \App\Models\ChatbotDesign $design
 * @property-read \App\Models\ChatbotFinish $finish
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ChatbotRequest[] $request
 * @property-read \App\Models\ChatbotSeo $seo
 * @property-read \App\Models\User $user
 */
final class Chatbot extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbots';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'user_id',
        'uuid',
        'title',
        'status',
        'domain',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'uuid' => 'string',
        'title' => 'string',
        'domain' => 'string',
        'status' => ChatbotStatusEnum::class,
        'has_page' => ChatbotHasPageEnum::class,
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function (self $model) {
            $model->uuid = Str::uuid()->toString();
        });
    }

    /**
     * @return Chatbot
     */
    public function toggleStatus(): self
    {
        if (ChatbotStatusEnum::ACTIVATED === (int)$this->status) {
            $this->update([
                'status' => ChatbotStatusEnum::DEACTIVATED(),
            ]);
        } elseif (ChatbotStatusEnum::DEACTIVATED === (int)$this->status) {
            $this->update([
                'status' => ChatbotStatusEnum::ACTIVATED(),
            ]);
        }

        return $this;
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasOne|null
     */
    public function design(): ?HasOne
    {
        return $this->hasOne(ChatbotDesign::class);
    }

    /**
     * @return HasOne|null
     */
    public function construction(): ?HasOne
    {
        return $this->hasOne(ChatbotConstruction::class);
    }

    /**
     * @return HasOne|null
     */
    public function finish(): ?HasOne
    {
        return $this->hasOne(ChatbotFinish::class);
    }

    /**
     * @return HasMany
     */
    public function request(): HasMany
    {
        return $this->hasMany(ChatbotRequest::class);
    }

    /**
     * @return HasOne|null
     */
    public function contactSetting(): ?HasOne
    {
        return $this->hasOne(ChatbotContactSetting::class);
    }

    /**
     * @return HasOne|null
     */
    public function page(): ?HasOne
    {
        return $this->hasOne(ChatbotPage::class);
    }

    /**
     * @return HasOne|null
     */
    public function integration(): ?HasOne
    {
        return $this->hasOne(ChatbotIntegration::class);
    }

    /**
     * Get an integration of the user
     *
     * @return HasOne|null
     */
    public function serviceIntegrations(): ?HasOne
    {
        return $this->hasOne(ChatbotServiceIntegration::class);
    }

}
