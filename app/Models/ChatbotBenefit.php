<?php

declare(strict_types=1);

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


/**
 * Class ChatbotBenefit
 *
 * @package App\Models\ChatbotBenefit
 *
 * @property integer $id
 * @property integer $chatbot_page_id
 * @property string $text
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read \App\Models\Chatbot $chatbot
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotBenefit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotBenefit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotBenefit query()
 * @mixin \Eloquent
 */
final class ChatbotBenefit extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbot_benefits';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_page_id',
        'text',
        'image',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_page_id' => 'integer',
        'text' => 'string',
        'image' => 'string',
    ];

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }
}
