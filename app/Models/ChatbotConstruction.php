<?php

declare(strict_types=1);

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


/**
 * App\Models\ChatbotConstruction
 *
 * @property integer $id
 * @property integer $chatbot_id
 * @property array $construction
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read \App\Models\Chatbot $chatbot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotConstruction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotConstruction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotConstruction query()
 *
 * @mixin \Eloquent
 */
final class ChatbotConstruction extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbot_constructions';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_id',
        'construction',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_id' => 'integer',
        'construction' => 'array',
    ];

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }
}
