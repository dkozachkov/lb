<?php

declare(strict_types=1);

namespace App\Models;


use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingEmailStatusEnum;
use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingNameStatusEnum;
use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingPhoneStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ChatbotContactSetting
 *
 * @property int $id
 * @property int $chatbot_id
 * @property ChatbotContactSettingPhoneStatusEnum $phone_status
 * @property string $phone_hints
 * @property string $phone_mask
 * @property ChatbotContactSettingNameStatusEnum $name_status
 * @property string $name_hints
 * @property ChatbotContactSettingEmailStatusEnum $email_status
 * @property string $email_hints
 * @property string $message
 *
 * @property-read \App\Models\Chatbot $chatbot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotContactSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotContactSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotContactSetting query()
 *
 * @mixin \Eloquent
 */
final class ChatbotContactSetting extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'chatbot_contact_settings';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_id',
        'phone_status',
        'phone_hints',
        'phone_mask',
        'name_status',
        'name_hints',
        'email_status',
        'email_hints',
        'message',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_id' => 'integer',
        'phone_status' => ChatbotContactSettingPhoneStatusEnum::class,
        'phone_hints' => 'string',
        'phone_mask' => 'string',
        'name_status' => ChatbotContactSettingNameStatusEnum::class,
        'name_hints' => 'string',
        'email_status' => ChatbotContactSettingEmailStatusEnum::class,
        'email_hints' => 'string',
        'message' => 'string',
    ];

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }

}
