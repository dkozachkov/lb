<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


/**
 * App\Models\ChatbotDesign
 *
 * @property integer $id
 * @property string $title
 * @property string $avatar
 * @property string $background
 * @property string $question_color
 * @property string $answer_color
 * @property string $button_color
 * @property string $font_question_color
 * @property string $font_answer_color
 * @property string $background_color
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read \App\Models\Chatbot $chatbot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotDesign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotDesign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotDesign query()
 *
 * @mixin \Eloquent
 */
final class ChatbotDesign extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbot_designs';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'title',
        'avatar',
        'background',
        'question_color',
        'answer_color',
        'button_color',
        'font_question_color',
        'font_answer_color',
        'background_color',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'avatar' => 'string',
        'background' => 'string',
        'question_color' => 'string',
        'answer_color' => 'string',
        'button_color' => 'string',
        'font_question_color' => 'string',
        'font_answer_color' => 'string',
        'background_color' => 'string',
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function (self $design) {
            $design->question_color = config('design.question_color');
            $design->answer_color = config('design.answer_color');
            $design->button_color = config('design.button_color');
            $design->font_question_color = config('design.font_question_color');
            $design->font_answer_color = config('design.font_answer_color');
            $design->background_color = config('design.background_color');
        });
    }

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }

}
