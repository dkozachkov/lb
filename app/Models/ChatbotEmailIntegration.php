<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class ChatbotEmailIntegration
 *
 * @package App\Models
 *
 * @property int $id
 * @property int $chatbot_service_id
 * @property string $email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
final class ChatbotEmailIntegration extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'chatbot_email_integrations';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_service_id',
        'email',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_service_id' => 'integer',
        'email' => 'string',
    ];

    /**
     * Get an integration entity
     *
     * @return BelongsTo
     */
    public function integration(): BelongsTo
    {
        return $this->belongsTo(ChatbotServiceIntegration::class, 'chatbot_service_id');
    }
}
