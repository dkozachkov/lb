<?php

declare(strict_types=1);

namespace App\Models;


use App\Enums\Chatbot\ChatbotFinish\ChatbotFinishMessageStatusEnum;
use App\Enums\Chatbot\ChatbotFinish\ChatbotFinishRedirectUrlStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ChatbotFinish
 *
 * @property string $id
 * @property string $chatbot_id
 * @property string $message
 * @property string $sub_message
 * @property ChatbotFinishMessageStatusEnum $message_status
 * @property string $redirect_url
 * @property ChatbotFinishRedirectUrlStatusEnum $redirect_url_status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property-read \App\Models\Chatbot $chatbot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotFinish newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotFinish newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotFinish query()
 *
 * @mixin \Eloquent
 */
final class ChatbotFinish extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbot_finishes';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_id',
        'message',
        'message_status',
        'sub_message',
        'redirect_url',
        'redirect_url_status',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_id' => 'integer',
        'message' => 'string',
        'sub_message' => 'string',
        'message_status' => ChatbotFinishMessageStatusEnum::class,
        'redirect_url' => 'string',
        'redirect_url_status' => ChatbotFinishRedirectUrlStatusEnum::class,
    ];

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }
}
