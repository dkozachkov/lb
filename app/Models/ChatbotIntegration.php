<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class ChatbotIntegration
 *
 * @package App\Models
 *
 * @property int $id
 * @property int $chatbotId
 * @property string $button_color
 * @property string $text_color
 * @property string $button_text
 */
final class ChatbotIntegration extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbot_integrations';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'button_color',
        'text_color',
        'button_text',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_id' => 'integer',
        'button_color' => 'string',
        'text_color' => 'string',
        'button_text' => 'string',
    ];

    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }

}
