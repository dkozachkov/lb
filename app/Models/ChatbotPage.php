<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * Class ChatbotPage
 *
 * @package App\Models
 *
 * @property integer $id
 * @property integer $chatbot_id
 * @property string $avatar
 * @property string $background
 * @property string $favicon
 * @property string $title
 * @property string $sub_title
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
final class ChatbotPage extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbot_pages';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_id',
        'avatar',
        'background',
        'favicon',
        'title',
        'sub_title',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_id' => 'integer',
        'avatar' => 'string',
        'background' => 'string',
        'favicon' => 'string',
        'title' => 'string',
        'sub_title' => 'string',
    ];

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }

    /**
     * @return HasMany
     */
    public function benefits(): HasMany
    {
        return $this->hasMany(ChatbotBenefit::class);
    }

    /**
     * @return HasOne|null
     */
    public function seo(): ?HasOne
    {
        return $this->hasOne(ChatbotSeo::class);
    }

}
