<?php

declare(strict_types=1);

namespace App\Models;


use App\Enums\Chatbot\ChatbotRequestStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\ChatbotRequest
 *
 * @property integer $id
 * @property integer $chatbot_id
 * @property string $phone
 * @property string $name
 * @property string $email
 * @property array $messages
 * @property ChatbotRequestStatusEnum $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read \App\Models\Chatbot $chatbot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotRequest query()
 *
 * @mixin \Eloquent
 */
final class ChatbotRequest extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbot_requests';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_id',
        'phone',
        'name',
        'email',
        'status',
        'messages',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_id' => 'integer',
        'phone' => 'string',
        'name' => 'string',
        'email' => 'string',
        'messages' => 'array',
        'status' => ChatbotRequestStatusEnum::class,
    ];

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }

}
