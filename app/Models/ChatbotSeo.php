<?php

declare(strict_types=1);

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\ChatbotSeo
 *
 * @property integer $id
 * @property integer $chatbot_page_id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $policy_url
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read \App\Models\Chatbot $chatbot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotSeo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotSeo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatbotSeo query()
 *
 * @mixin \Eloquent
 */
final class ChatbotSeo extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'chatbot_seo';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_page_id',
        'title',
        'description',
        'keywords',
        'policy_url',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_page_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'keywords' => 'string',
        'policy_url' => 'string',
    ];

    /**
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        return $this->belongsTo(Chatbot::class);
    }
}
