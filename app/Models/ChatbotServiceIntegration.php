<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Enums\Integration\IntegrationEmailStatusEnum;

final class ChatbotServiceIntegration extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'chatbot_service_integrations';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'chatbot_id',
        'email_theme',
        'email_status'
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'chatbot_id' => 'integer',
        'email_theme' => 'string',
        'email_status' => IntegrationEmailStatusEnum::class
    ];

    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * Get an owner
     *
     * @return BelongsTo
     */
    public function chatbot(): BelongsTo
    {
        $this->belongsTo(Chatbot::class);
    }

    /**
     * Get emails
     *
     * @return HasMany
     */
    public function emails(): HasMany
    {
        return $this->hasMany(ChatbotEmailIntegration::class, 'chatbot_service_id');
    }
}
