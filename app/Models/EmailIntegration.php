<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class EmailIntegration
 *
 * @package App\Models
 *
 * @property int $id
 * @property int $service_integration_id
 * @property string $email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
final class EmailIntegration extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'email_integrations';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'service_integration_id',
        'email',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'service_integration_id' => 'integer',
        'email' => 'string',
    ];

    /**
     * Get an integration entity
     *
     * @return BelongsTo
     */
    public function integration(): BelongsTo
    {
        return $this->belongsTo(ServiceIntegration::class, 'service_integration_id');
    }
}
