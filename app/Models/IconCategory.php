<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class IconCategory
 *
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $path
 */
final class IconCategory extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'icon_categories';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'title', 'path',
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'path' => 'string',
    ];

    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * Get all icons of the category
     *
     * @return array
     */
    public function getIcons(): array
    {
        return Storage::disk('public')->files($this->path);
    }

}
