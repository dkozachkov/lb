<?php

namespace App\Models;

use App\Enums\Integration\IntegrationEmailStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ServiceIntegration
 *
 * @package App\Models
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $email_theme
 * @property IntegrationEmailStatusEnum $email_status
 */
final class ServiceIntegration extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'service_integrations';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'user_id',
        'email_theme',
        'email_status'
    ];

    /**
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'email_theme' => 'string',
        'email_status' => IntegrationEmailStatusEnum::class
    ];

    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * Get an owner
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        $this->belongsTo(User::class);
    }

    /**
     * Get emails
     *
     * @return HasMany
     */
    public function emails(): HasMany
    {
        return $this->hasMany(EmailIntegration::class);
    }
}
