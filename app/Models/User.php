<?php

declare(strict_types=1);

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Passport\HasApiTokens;


/**
 * Class User
 *
 * @package App\Models\User
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $avatar
 * @property string $password
 * @property string $provider_facebook
 * @property string $provider_google
 * @property string $provider_vkontakte
 * @property string $email_verified_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chatbot[] $chatbots
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 *
 * @mixin \Eloquent
 *
 */
final class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * @var string $table
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array $fillable
     */
    protected $fillable = [
        'name', 'email', 'password',
        'provider_facebook', 'provider_google',
        'provider_vkontakte', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array $hidden
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array $casts
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'provider_facebook' => 'string',
        'provider_google' => 'string',
        'provider_vkontakte' => 'string',
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasMany
     */
    public function chatbots(): HasMany
    {
        return $this->hasMany(Chatbot::class);
    }

    /**
     * Get an integration of the user
     *
     * @return HasOne|null
     */
    public function serviceIntegrations(): ?HasOne
    {
        return $this->hasOne(ServiceIntegration::class);
    }

}
