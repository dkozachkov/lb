<?php

declare(strict_types=1);

namespace App\Providers;


use App\Services\Integration\ServiceIntegrationEmailService;
use App\Services\Integration\ServiceIntegrationEmailServiceContract;
use Illuminate\Support\ServiceProvider;
use App\Services\Chatbot\{ChatbotService, ChatbotServiceContract, Integration\ChatbotServiceIntegrationEmailService};
use App\Services\ChatbotPage\{ChatbotPageService, ChatbotPageServiceContract};
use App\Services\ChatbotSetting\{ChatbotSettingService, ChatbotSettingServiceContract};
use App\Services\ChatbotIntegration\{ChatbotIntegrationService, ChatbotIntegrationServiceContract};
use App\Services\ChatbotConstruction\{ChatbotConstructionService, ChatbotConstructionServiceContract};

/**
 * Class InjectionServiceProvider
 *
 * @package App\Providers
 */
final class InjectionServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(ChatbotServiceContract::class, ChatbotService::class);
        app()->bind(ChatbotSettingServiceContract::class, ChatbotSettingService::class);
        app()->bind(ChatbotPageServiceContract::class, ChatbotPageService::class);
        app()->bind(ChatbotIntegrationServiceContract::class, ChatbotIntegrationService::class);
        app()->bind(ChatbotConstructionServiceContract::class, ChatbotConstructionService::class);

        app()->bind(ServiceIntegrationEmailServiceContract::class, function () {
            $chatbotId = request('chatbotId');
            if ($chatbotId) {
                return new ChatbotServiceIntegrationEmailService((int)$chatbotId);
            }

            return new ServiceIntegrationEmailService;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
