<?php

namespace App\Rules;


use Illuminate\Contracts\Validation\Rule;

/**
 * Class DomainRule
 *
 * @package App\Rules
 */
final class DomainRule implements Rule
{

    /**
     * @var string $rule
     */
    private $rule = "/^(http[s]?\:\/\/)?((\w+)\.)?(([\w-]+)?)(\.[\w-]+){1,2}$/";

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (bool)preg_match($this->rule, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('rules.domain');
    }
}
