<?php
/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 22.08.2019
 * Time: 11:35
 */

namespace App\Services;


/**
 * Class BaseService
 *
 * @package App\Services
 */
abstract class BaseService
{

    /**
     * @var \App\Models\User $auth
     */
    protected $auth;

    /**
     * @param $auth
     */
    public function setUser($auth)
    {
        $this->auth = $auth;
    }

}