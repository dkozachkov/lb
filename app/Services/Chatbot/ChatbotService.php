<?php
/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 19.08.2019
 * Time: 15:23
 */

namespace App\Services\Chatbot;


use App\Enums\Chatbot\ChatbotRequestStatusEnum;
use App\Models\Chatbot;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\{Builder, Model};
use ReflectionClass;
use ReflectionMethod;

/**
 * Class ChatbotService
 *
 * @package App\Services\Chatbot
 */
final class ChatbotService extends BaseService implements ChatbotServiceContract
{

    /**
     * @var array $types
     */
    private $types = [
        'Illuminate\Database\Eloquent\Relations\HasMany',
        'Illuminate\Database\Eloquent\Relations\HasOne'
    ];

    /**
     * @param array $params
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getChatbots(array $params)
    {
        /**
         * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $chatbots
         */
        $chatbots = $this->auth
            ->chatbots()
            ->withCount(['request as request_count' => function (Builder $query) {
                $query->where('status', ChatbotRequestStatusEnum::ACTIVATED());
            }])
            ->when(!empty($params) && isset($params['search']), function (Builder $query) use ($params) {
                $query->where('title', 'like', "%{$params['search']}%");
            })->orderByDesc('id')
            ->paginate($params['count'] ?? 10);

        /**
         * @var array $chatbotIds
         */
        /*$chatbotIds = $this->auth->chatbots()
            ->get(['id'])
            ->pluck('id')
            ->toArray();

        ChatbotRequest::whereIn('id', $chatbotIds)
            ->update([
                'status' => ChatbotRequestStatusEnum::DEACTIVATED()
            ]);*/

        return $chatbots;
    }

    /**
     * @param int $chatbotId
     *
     * @return Chatbot
     */
    public function getChatbot(int $chatbotId): Chatbot
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->auth->chatbots()
            ->where('id', $chatbotId)
            ->firstOrFail();

        return $chatbot;
    }

    /**
     * @param array $params
     *
     * @return Chatbot
     *
     * @throws \Throwable
     */
    public function store(array $params): Chatbot
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = \DB::transaction(function () use ($params) {
            /**
             * @var Chatbot $chatbot
             */
            $params['user_id'] = 2;
            $chatbot = $this->auth->chatbots()
                ->create($params);

            $this->makeChatbotFinish($chatbot);
            $this->makeContactSetting($chatbot);
            $this->makeChatbotDesign($chatbot);
            $this->makeChatbotConstructor($chatbot);
            $this->makeServiceIntegration($chatbot);

            return $chatbot;
        }, 3);

        return $chatbot;
    }

    /**
     * @param Chatbot $chatbot
     */
    private function makeChatbotFinish(Chatbot $chatbot): void
    {
        $chatbot->finish()->create([
            'message' => '',
            'sub_message' => ''
        ]);
    }

    /**
     * @param Chatbot $chatbot
     */
    private function makeContactSetting(Chatbot $chatbot): void
    {
        $chatbot->contactSetting()->create([
            'message' => ''
        ]);
    }

    /**
     * @param Chatbot $chatbot
     */
    private function makeChatbotDesign(Chatbot $chatbot): void
    {
        $chatbot->design()->create([
            'question_color' => config('design.question_color'),
            'answer_color' => config('design.answer_color'),
            'button_color' => config('design.button_color'),
            'font_question_color' => config('design.font_question_color'),
            'font_answer_color' => config('design.font_answer_color'),
            'background_color' => config('design.background_color'),
        ]);
    }

    /**
     * @param Chatbot $chatbot
     */
    private function makeChatbotConstructor(Chatbot $chatbot): void
    {
        $chatbot->construction()->create([
            'construction' => []
        ]);
    }

    /**
     * @param Chatbot $chatbot
     */
    private function makeServiceIntegration(Chatbot $chatbot): void
    {
        /**
         * @var \App\Models\ChatbotServiceIntegration $serviceIntegration
         */
        $serviceIntegration = $chatbot->serviceIntegrations()->create([
            'email_theme' => config('service_integration.email_theme')
        ]);

        /**
         * @var \App\Models\User $user
         */
        $user = $chatbot->user()->firstOrFail();

        $serviceIntegration->emails()->create([
            'email' => $user->email,
        ]);
    }

    /**
     * @param int $chatbotId
     * @param array $params
     *
     * @return Chatbot
     */
    public function update(int $chatbotId, array $params): Chatbot
    {
        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        $chatbot->update($params);

        return $chatbot;
    }

    /**
     * @param int $chatbotId
     *
     * @throws \Exception
     */
    public function delete(int $chatbotId): void
    {
        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        $chatbot->delete();
    }

    /**
     * @param int $chatbotId
     *
     * @return Chatbot
     *
     * @throws \Throwable
     */
    public function duplicate(int $chatbotId): Chatbot
    {

        /**
         * @var Chatbot $chatbot
         */
        $newChatbot = \DB::transaction(function () use ($chatbotId) {
            /**
             * @var Chatbot $chatbot
             */
            $chatbot = $this->getChatbot($chatbotId);

            /**
             * @var Chatbot $chatbot
             */
            $replicatedChatbot = $chatbot->replicate();
            $replicatedChatbot->title .= ' (1)';
            $replicatedChatbot->push();

            $this->replicateRelations($chatbot, $replicatedChatbot);

            return $replicatedChatbot;
        }, 3);

        return $newChatbot;

    }

    /**
     * @param $model
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    private function getModelRelations($model): array
    {
        $methodNames = [];
        $methods = (new ReflectionClass($model))->getMethods(ReflectionMethod::IS_PUBLIC);

        foreach ($methods as $method) {
            if (in_array(optional($method->getReturnType())->getName(), $this->types)) {
                $methodNames[] = $method->name;
            }
        }

        return $methodNames;
    }

    /**
     * @param $entry
     *
     * @return bool
     */
    private function isCollection($entry): bool
    {
        return get_class($entry) === \Illuminate\Database\Eloquent\Collection::class;
    }

    /**
     * @param Model $replicatedModel
     * @param string $relation
     * @param Model $entry
     *
     * @throws \ReflectionException
     */
    private function replicateModel(Model $replicatedModel, string $relation, Model $entry)
    {
        $relations = $this->getModelRelations($entry);
        $entry->load($relations);

        /**
         * @var Model $newModel
         */
        $newModel = $entry->replicate();
        if (isset($newModel->title)) {
            $newModel->title .= ' (1)';
        }

        if ($newModel->push()) {
            $replicatedModel->{$relation}()->save($newModel);
            $this->replicateRelations($entry, $newModel);
        }
    }

    /**
     * @param Model $model
     * @param Model $replicatedModel
     *
     * @throws \ReflectionException
     */
    private function replicateRelations(Model $model, Model $replicatedModel)
    {
        /**
         * @var array $modelRelations
         */
        $modelRelations = $this->getModelRelations($model);
        $model->load($modelRelations);
        $modelRelations = $model->getRelations();

        /**
         * @var \Illuminate\Database\Eloquent\Model $entry
         */
        foreach ($modelRelations as $relation => $entry) {
            if (empty($entry) || $relation === 'request') {
                continue;
            }

            if ($this->isCollection($entry)) {
                foreach ($entry as $e) {
                    $this->replicateModel($replicatedModel, $relation, $e);
                }
            } else {
                $this->replicateModel($replicatedModel, $relation, $entry);
            }
        }
    }

    /**
     * @param int $chatbotId
     *
     * @return Chatbot
     */
    public function toggleStatus(int $chatbotId): Chatbot
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        $chatbot->toggleStatus();

        return $chatbot;
    }

    /**
     * @param int $chatbotId
     * @param array $params
     *
     * @return Chatbot
     */
    public function changeTitle(int $chatbotId, array $params): Chatbot
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->auth->chatbots()
            ->findOrFail($chatbotId);

        $chatbot->update($params);

        return $chatbot;
    }


}
