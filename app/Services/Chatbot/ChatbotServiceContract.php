<?php
/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 19.08.2019
 * Time: 15:23
 */

namespace App\Services\Chatbot;


use App\Models\Chatbot;

/**
 * Interface ChatbotServiceContract
 *
 * @package App\Services\Chatbot
 * @method setUser($auth)
 */
interface ChatbotServiceContract
{

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function getChatbots(array $params);

    /**
     * @param int $chatbotId
     *
     * @return Chatbot
     */
    public function getChatbot(int $chatbotId): Chatbot;

    /**
     * @param array $params
     *
     * @return Chatbot
     */
    public function store(array $params): Chatbot;

    /**
     * @param int $chatbotId
     * @param array $params
     *
     * @return Chatbot
     */
    public function update(int $chatbotId, array $params): Chatbot;

    /**
     * @param int $chatbotId
     *
     * @return void
     */
    public function delete(int $chatbotId): void;

    /**
     * @param int $chatbotId
     *
     * @return Chatbot
     */
    public function duplicate(int $chatbotId): Chatbot;

    /**
     * @param int $chatbotId
     *
     * @return Chatbot
     */
    public function toggleStatus(int $chatbotId): Chatbot;

    /**
     * @param int $chatbotId
     * @param array $params
     *
     * @return Chatbot
     */
    public function changeTitle(int $chatbotId, array $params): Chatbot;

}
