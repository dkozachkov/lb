<?php

namespace App\Services\Chatbot\Integration;

use App\Models\{Chatbot, ChatbotServiceIntegration, ServiceIntegration};
use App\Services\Integration\ServiceIntegrationEmailServiceContract;

/**
 * Class ChatbotServiceIntegrationEmailService
 *
 * @package App\Services\Chatbot\Integration
 */
final class ChatbotServiceIntegrationEmailService implements ServiceIntegrationEmailServiceContract
{
    /**
     * @var int $chatbotId
     */
    private $chatbotId;

    /**
     * ChatbotServiceIntegrationEmailService constructor.
     *
     * @param int $chatbotId
     */
    public function __construct(int $chatbotId)
    {
        $this->chatbotId = $chatbotId;
    }

    /**
     * @return object
     */
    public function index()
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot();

        /**
         * @var \App\Models\ChatbotServiceIntegration $serviceIntegrations
         */
        $serviceIntegrations = $chatbot->serviceIntegrations()->firstOrFail();

        /**
         * @var array $emails
         */
        $emails = $this->getEmails($serviceIntegrations);

        return (object)[
            'title' => $serviceIntegrations->email_theme,
            'emails' => $emails,
            'email_status' => $serviceIntegrations->email_status,
        ];
    }

    /**
     * @param string $title
     * @param array $emails
     *
     * @return mixed|object
     */
    public function update(string $title, array $emails)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot();

        /**
         * @var \App\Models\ChatbotServiceIntegration $serviceIntegrations
         */
        $serviceIntegrations = $chatbot->serviceIntegrations()->firstOrFail();

        \DB::transaction(function () use ($serviceIntegrations, $title, $emails) {
            $serviceIntegrations->update([
                'email_theme' => $title
            ]);

            $serviceIntegrations->emails()->delete();

            foreach ($emails as $email) {
                $serviceIntegrations->emails()->create(['email' => $email]);
            }
        }, 3);

        /**
         * @var array $emails
         */
        $emails = $this->getEmails($serviceIntegrations);

        return (object)[
            'title' => $serviceIntegrations->email_theme,
            'emails' => $emails,
            'email_status' => $serviceIntegrations->email_status,
        ];
    }

    /**
     * @param array $params
     *
     * @return ServiceIntegration|mixed
     */
    public function changeStatus(array $params)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot();

        /**
         * @var \App\Models\ServiceIntegration $serviceIntegration
         */
        $serviceIntegration = $chatbot->serviceIntegrations()->firstOrFail();

        $serviceIntegration->update($params);

        return $serviceIntegration;
    }

    /**
     * @param ChatbotServiceIntegration $serviceIntegration
     *
     * @return array
     */
    private function getEmails(ChatbotServiceIntegration $serviceIntegration): array
    {
        return $serviceIntegration->emails()
            ->get()
            ->pluck('email')
            ->toArray();
    }

    /**
     * Return a chatbot by a chatbot id
     *
     * @return Chatbot
     */
    private function getChatbot(): Chatbot
    {
        /**
         * @var \App\Models\User $auth
         */
        $auth = \Auth::user();

        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $auth->chatbots()->findOrFail($this->chatbotId);

        return $chatbot;
    }
}
