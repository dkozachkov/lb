<?php


namespace App\Services\ChatbotConstruction;


use App\Models\Chatbot;
use App\Models\ChatbotConstruction;
use App\Services\BaseService;

/**
 * Class ChatbotConstructionService
 *
 * @package App\Services\ChatbotConstruction
 */
final class ChatbotConstructionService extends BaseService implements ChatbotConstructionServiceContract
{

    /**
     * @param int $chatbotId
     *
     * @return mixed
     */
    public function get(int $chatbotId)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->auth->chatbots()
            ->with(['design', 'construction', 'finish', 'contactSetting', 'page'])
            ->where('id', $chatbotId)
            ->firstOrFail();

        return $chatbot;
    }

    /**
     * @param int $chatbotId
     * @param array $params
     *
     * @return ChatbotConstruction
     */
    public function update(int $chatbotId, array $params): ChatbotConstruction
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->auth->chatbots()
            ->where('id', $chatbotId)
            ->firstOrFail();

        /**
         * @var ChatbotConstruction $construction
         */
        $construction = $chatbot->construction()
            ->firstOrFail();


        $construction->update($params);

        return $construction;
    }

    /**
     * @param $auth
     */
    public function setUser($auth)
    {
        parent::setUser($auth);
    }
}
