<?php


namespace App\Services\ChatbotConstruction;


/**
 * Interface ChatbotConstructionServiceContract
 *
 * @package App\Services\ChatbotConstruction
 */
interface ChatbotConstructionServiceContract
{

    /**
     * @param int $chatbotId
     *
     * @return mixed
     */
    public function get(int $chatbotId);

    /**
     * @param int $chatbotId
     * @param array $params
     *
     * @return mixed
     */
    public function update(int $chatbotId, array $params);

}
