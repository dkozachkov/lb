<?php

/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 20.08.2019
 * Time: 12:41
 */

namespace App\Services\ChatbotIntegration;


use App\Models\{Chatbot, ChatbotIntegration, ChatbotPage};
use App\Services\BaseService;

/**
 * Class ChatbotIntegrationService
 *
 * @package App\Services\ChatbotIntegration
 */
final class ChatbotIntegrationService extends BaseService implements ChatbotIntegrationServiceContract
{

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $auth
     */
    public function setUser($auth)
    {
        parent::setUser($auth);
    }

    /**
     * @param int $chatbotId
     *
     * @return array
     */
    public function showData(int $chatbotId): array
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = \Auth::user()->chatbots()
            ->findOrFail($chatbotId);

        /**
         * @var ChatbotPage $page
         */
        $page = $chatbot->page()->first();

        /**
         * @var ChatbotIntegration $integration
         */
        $integration = $chatbot->integration()->first();

        if (!$integration) {
            /**
             * @var ChatbotIntegration $integration
             */
            $integration = $chatbot->integration()->create([
                'button_color' => config('integration.integration_button_color'),
                'text_color' => config('integration.integration_button_text_color'),
                'button_text' => config('integration.integration_button_text'),
            ]);
        }

        $integration['domain'] = $chatbot->domain;

        return [
            'id' => $chatbot->id,
            'uuid' => $chatbot->uuid,
            'integration' => $integration,
            'page' => $page,
        ];
    }

    /**
     * @param int $chatbotId
     *
     * @return ChatbotIntegration|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasOne|mixed|object|null
     */
    public function show(int $chatbotId)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = \Auth::user()->chatbots()
            ->findOrFail($chatbotId);

        /**
         * @var ChatbotIntegration $integration
         */
        $integration = $chatbot->integration()->first();

        $integration['domain'] = $chatbot->domain;

        return $integration;
    }

    /**
     * @param int $chatbotId
     * @param array $params
     * @param string $domain
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasOne|mixed|object|null
     *
     * @throws \Throwable
     */
    public function update(int $chatbotId, array $params, string $domain)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->auth->chatbots()
            ->findOrFail($chatbotId);

        $integration = \DB::transaction(function () use ($chatbot, $params, $domain) {
            if ($domain != $chatbot->domain) {
                $chatbot->update(['domain' => $domain]);
            }

            /**
             * @var ChatbotIntegration $integration
             */
            $integration = $chatbot->integration()->first();

            if ($integration) {
                $chatbot->integration()->update($params);
            } else {
                $integration = $chatbot->integration()->create($params);
            }

            $integration->refresh();
            return $integration;
        }, 3);

        $integration['domain'] = $chatbot->domain;

        return $integration;
    }
}
