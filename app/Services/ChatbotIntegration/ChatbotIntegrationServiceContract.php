<?php
/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 20.08.2019
 * Time: 12:41
 */

namespace App\Services\ChatbotIntegration;


/**
 * Interface ChatbotPageServiceContract
 *
 * @package App\Services\ChatbotIntegration
 *
 * @method setUser(\Illuminate\Contracts\Auth\Authenticatable $auth)
 */
interface ChatbotIntegrationServiceContract
{

    /**
     * @param int $chatbotId
     *
     * @return array
     */
    public function showData(int $chatbotId): array;

    /**
     * @param int $chatbotId
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasOne|mixed|object|null
     */
    public function show(int $chatbotId);

    /**
     * @param int $chatbotId
     * @param array $params
     * @param string $domain
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasOne|mixed|object|null
     */
    public function update(int $chatbotId, array $params, string $domain);

}
