<?php
/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 20.08.2019
 * Time: 12:41
 */

namespace App\Services\ChatbotPage;


use App\Enums\Chatbot\ChatbotHasPageEnum;
use App\Models\{Chatbot, ChatbotPage};
use App\Services\BaseService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

/**
 * Class ChatbotPageService
 *
 * @package App\Services\ChatbotPage
 */
final class ChatbotPageService extends BaseService implements ChatbotPageServiceContract
{
    public function setUser($auth)
    {
        parent::setUser($auth);
    }

    /**
     * @param int $chatbotId
     *
     * @return ChatbotPage
     */
    public function show(int $chatbotId): ChatbotPage
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        /**
         * @var ChatbotPage $chatbotPage
         */
        $chatbotPage = $chatbot->page()
            ->with(['benefits', 'seo'])
            ->firstOrFail();

        return $chatbotPage;
    }

    public function store(int $chatbotId, array $params, array $benefits, array $seo)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        $this->setImagePath($params, 'background');
        $this->setImagePath($params, 'avatar');
        $this->setImagePath($params, 'favicon');

        /**
         * @var ChatbotPage $chatbotPage
         */
        $chatbotPage = \DB::transaction(function () use ($chatbot, $params, $benefits, $seo) {

            /**
             * @var ChatbotPage $chatbotPage
             */
            $chatbotPage = $this->createChatbotPage($chatbot, $params);

            $this->createBenefits($chatbotPage, $benefits);
            $this->createSeo($chatbotPage, $seo);

            $chatbot->has_page = ChatbotHasPageEnum::HAS();
            $chatbot->save();

            return $chatbotPage;
        }, 3);

        return $chatbotPage;
    }

    /**
     * @param Chatbot $chatbot
     * @param array $params
     *
     * @return ChatbotPage
     */
    private function createChatbotPage(Chatbot $chatbot, array $params): ChatbotPage
    {
        if ($chatbot->page()->exists()) {
            $chatbot->page()->delete();
        }

        return $chatbot->page()->create($params);
    }

    /**
     * @param ChatbotPage $chatbotPage
     * @param array $benefits
     *
     * @return void
     */
    private function createBenefits(ChatbotPage $chatbotPage, array $benefits): void
    {
        if ($chatbotPage->benefits()->exists()) {
            $chatbotPage->benefits()->delete();
        }

        foreach ($benefits as $benefit) {
            $chatbotPage->benefits()->create($benefit);
        }
    }

    /**
     * @param ChatbotPage $chatbotPage
     * @param array $seo
     *
     * @return void
     */
    private function createSeo(ChatbotPage $chatbotPage, array $seo): void
    {
        if ($chatbotPage->seo()->exists()) {
            $chatbotPage->seo()->delete();
        }

        $chatbotPage->seo()->create($seo);
    }

    /**
     * @param int $chatbotId
     * @param array $params
     * @param array $benefits
     * @param array $seo
     *
     * @return mixed
     *
     * @throws \Throwable
     */
    public function update(int $chatbotId, array $params, array $benefits, array $seo): ChatbotPage
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = $this->getChatbot($chatbotId);

        $this->setImagePath($params, 'background');
        $this->setImagePath($params, 'avatar');
        $this->setImagePath($params, 'favicon');

        /**
         * @var ChatbotPage $chatbotPage
         */
        $chatbotPage = $chatbot->page()->firstOrFail();

        \DB::transaction(function () use ($chatbotPage, $params, $benefits, $seo) {
            $chatbotPage->update($params);

            if (!empty($benefits)) {
                $this->createBenefits($chatbotPage, $benefits);
            }

            if (!empty($seo)) {
                $this->createSeo($chatbotPage, $seo);
            }

        }, 3);

        return $chatbotPage;
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    private function uploadImage(UploadedFile $file): string
    {
        /**
         * @var string $tempFilename
         */
        $tempFilename = Str::random();

        /**
         * @var string $ext
         */
        $ext = $file->getClientOriginalExtension();

        /**
         * @var string $filename
         */
        $filename = "$tempFilename.$ext";

        /**
         * @var string $path
         */
        $path = $file->storeAs('public/images', $filename);

        return '/' . str_replace('public', 'storage', $path);
    }

    /**
     * @param array $params
     * @param string $key
     * @param bool $unset
     */
    private function setImagePath(array &$params, string $key, bool $unset = false): void
    {
        if (isset($params[$key]) && !empty($params[$key])) {
            if (gettype($params[$key]) !== 'string') {
                $params[$key] = $this->uploadImage($params[$key]);
            }
        } else {
            if ($unset) {
                unset($params[$key]);
            } else {
                $params[$key] = '';
            }
        }
    }

    /**
     * @param int $chatbotId
     *
     * @return Chatbot|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    private function getChatbot(int $chatbotId): Chatbot
    {
        return \Auth::user()->chatbots()
            ->findOrFail($chatbotId);
    }

}
