<?php
/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 20.08.2019
 * Time: 12:41
 */

namespace App\Services\ChatbotPage;


/**
 * Interface ChatbotPageServiceContract
 *
 * @package App\Services\ChatbotPage
 * @method setUser(\Illuminate\Contracts\Auth\Authenticatable $auth)
 */
interface ChatbotPageServiceContract
{

    /**
     * @param int $chatbotId
     *
     * @return mixed
     */
    public function show(int $chatbotId);

    /**
     * @param int $chatbotId
     * @param array $params
     * @param array $benefits
     * @param array $seo
     *
     * @return mixed
     */
    public function store(int $chatbotId, array $params, array $benefits, array $seo);

    /**
     * @param int $chatbotId
     * @param array $params
     * @param array $benefits
     * @param array $seo
     *
     * @return mixed
     */
    public function update(int $chatbotId, array $params, array $benefits, array $seo);

}