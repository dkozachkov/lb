<?php

/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 19.08.2019
 * Time: 15:23
 */

namespace App\Services\ChatbotSetting;


use App\Models\{Chatbot, ChatbotContactSetting, ChatbotDesign, ChatbotFinish};
use App\Services\BaseService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

/**
 * Class ChatbotSettingService
 *
 * @package App\Services\ChatbotSetting
 */
final class ChatbotSettingService extends BaseService implements ChatbotSettingServiceContract
{

    public function setUser($auth)
    {
        parent::setUser($auth);
    }

    /**
     * @param int $chatbotId
     *
     * @return ChatbotContactSetting|mixed
     */
    public function show(int $chatbotId)
    {
        /**
         * @var Chatbot $chatbot
         */
        $chatbot = \Auth::user()->chatbots()
            ->with(['contactSetting', 'design', 'finish'])
            ->findOrFail($chatbotId);

        if (isset($chatbot->finish) && is_null($chatbot->finish->sub_message)) {
            $chatbot->finish->sub_message = "";
        }

        return $chatbot;
    }

    /**
     * @param int $chatbotId
     * @param array $params
     * @param array $designData
     * @param array $contactData
     * @param array $finishData
     *
     * @return Chatbot
     *
     * @throws \Throwable
     */
    public function update(
        int $chatbotId,
        array $params,
        array $designData,
        array $contactData,
        array $finishData
    ): Chatbot
    {
        return \DB::transaction(function () use ($params, $designData, $contactData, $finishData, $chatbotId) {
            /**
             * @var Chatbot $chatbot
             */
            $chatbot = Chatbot::where('id', $chatbotId)
                ->firstOrFail();

            /**
             * @var ChatbotDesign $design
             */
            $design = $chatbot->design()->first();
            if (!$design) {
                $design = $chatbot->design()->create([]);
            }

            /**
             * @var ChatbotFinish $finish
             */
            $finish = $chatbot->finish()->first();
            if (!$finish) {
                $finish = $chatbot->finish()->create(['message' => '', 'sub_message' => '']);
            }

            /**
             * @var ChatbotContactSetting $contact
             */
            $contact = $chatbot->contactSetting()->first();
            if (!$contact) {
                $contact = $chatbot->contactSetting()->create([]);
            }

            $this->setDesignImages($designData);

            $chatbot->update($params);
            $design->update($designData);
            $contact->update($contactData);
            $finish->update($finishData);

            $chatbot->load(['design', 'finish', 'contactSetting']);

            return $chatbot;
        }, 3);

    }

    /**
     * @param array $data
     */
    private function setDesignImages(array &$data): void
    {
        $this->setImagePath($data, 'background');
        $this->setImagePath($data, 'avatar');
    }

    /**
     * @param array $params
     * @param string $key
     */
    private function setImagePath(array &$params, string $key): void
    {
        if (isset($params[$key]) && !empty($params[$key])) {
            if (gettype($params[$key]) !== 'string') {
                $params[$key] = $this->uploadImage($params[$key]);
            }
        } else {
            $data[$key] = '';
        }
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    private function uploadImage(UploadedFile $file): string
    {
        /**
         * @var string $tempFilename
         */
        $tempFilename = Str::random();

        /**
         * @var string $ext
         */
        $ext = $file->getClientOriginalExtension();

        /**
         * @var string $filename
         */
        $filename = "$tempFilename.$ext";

        /**
         * @var string $path
         */
        $path = $file->storeAs('public/images', $filename);

        return '/' . str_replace('public', 'storage', $path);
    }
}
