<?php
/**
 * Created by PhpStorm.
 * User: 38095
 * Date: 19.08.2019
 * Time: 15:23
 */

namespace App\Services\ChatbotSetting;


/**
 * Interface ChatbotSettingServiceContract
 *
 * @package App\Services\ChatbotSetting
 * @method setUser(\Illuminate\Contracts\Auth\Authenticatable $auth)
 */
interface ChatbotSettingServiceContract
{

    /**
     * @param int $chatbotId
     *
     * @return mixed
     */
    public function show(int $chatbotId);

    /**
     * @param int $chatbotId
     * @param array $params
     * @param array $design
     * @param array $contact
     * @param array $finish
     *
     * @return mixed
     */
    public function update(int $chatbotId, array $params, array $design, array $contact, array $finish);

}
