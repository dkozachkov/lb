<?php


namespace App\Services\Integration;


use App\Models\ChatbotServiceIntegration;
use App\Models\ServiceIntegration;

final class ServiceIntegrationEmailService implements ServiceIntegrationEmailServiceContract
{
    /**
     * @return object
     */
    public function index()
    {
        /**
         * @var \App\Models\User $auth
         */
        $auth = \Auth::user();

        /**
         * @var \App\Models\ServiceIntegration $serviceIntegrations
         */
        $serviceIntegrations = $auth->serviceIntegrations()->firstOrFail();

        /**
         * @var array $emails
         */
        $emails = $this->getEmails($serviceIntegrations);

        return (object)[
            'title' => $serviceIntegrations->email_theme,
            'emails' => $emails,
            'email_status' => $serviceIntegrations->email_status,
        ];
    }

    public function update(string $title, array $emails)
    {
        /**
         * @var \App\Models\User $auth
         */
        $auth = \Auth::user();

        /**
         * @var \App\Models\ServiceIntegration $serviceIntegrations
         */
        $serviceIntegrations = $auth->serviceIntegrations()->firstOrFail();

        \DB::transaction(function () use ($serviceIntegrations, $title, $emails) {
            $serviceIntegrations->update([
                'email_theme' => $title
            ]);

            $serviceIntegrations->emails()->delete();

            foreach ($emails as $email) {
                $serviceIntegrations->emails()->create(['email' => $email]);
            }
        }, 3);

        /**
         * @var array $emails
         */
        $emails = $this->getEmails($serviceIntegrations);

        return (object)[
            'title' => $serviceIntegrations->email_theme,
            'emails' => $emails,
            'email_status' => $serviceIntegrations->email_status,
        ];
    }

    /**
     * @param ServiceIntegration $serviceIntegration
     *
     * @return array
     */
    private function getEmails(ServiceIntegration $serviceIntegration): array
    {
        return $serviceIntegration->emails()
            ->get()
            ->pluck('email')
            ->toArray();
    }

    /**
     * @param array $params
     *
     * @return ServiceIntegration|mixed
     */
    public function changeStatus(array $params)
    {
        /**
         * @var \App\Models\User $auth
         */
        $auth = \Auth::user();

        return \DB::transaction(function () use ($auth, $params) {
            /**
             * @var \App\Models\ServiceIntegration $serviceIntegration
             */
            $serviceIntegration = $auth->serviceIntegrations()->firstOrFail();

            $serviceIntegration->update($params);

            /**
             * @var array $chatbotIds
             */
            $chatbotIds = $auth->chatbots()
                ->get('id')
                ->pluck('id')
                ->toArray();

            ChatbotServiceIntegration::whereIn('id', $chatbotIds)
                ->update($params);

            return $serviceIntegration;
        }, 3);
    }


}
