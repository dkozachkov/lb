<?php


namespace App\Services\Integration;


interface ServiceIntegrationEmailServiceContract
{

    public function index();

    /**
     * @param string $title
     * @param array $emails
     *
     * @return mixed
     */
    public function update(string $title, array $emails);

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function changeStatus(array $params);

}
