<?php

if (!function_exists('get_domain_from_url')) {
    function get_domain_from_url(string $url) {
        if (!empty($url)) {
            if (gettype(strpos($url, 'http://')) === 'integer') {
                $url = str_replace('http://', '', $url);
            } elseif (gettype(strpos($url, 'https://')) === 'integer') {
                $url = str_replace('https://', '', $url);
            }
        }

        return explode('/', $url)[0];
    }
}

if (!function_exists('prepare_param')) {
    /**
     * @param array $params
     * @param string $param
     *
     * @return void
     */
    function prepare_param(array &$params, $param): void {
        /**
         * @var bool $paramNotExists
         */
        $paramNotExists = !isset($params[$param]);

        /**
         * @var bool $paramIsEmpty
         */
        $paramIsEmpty = isset($params[$param]) && empty($params[$param]);

        if ($paramNotExists || $paramIsEmpty) {
            $params[$param] = '';
        }
    }
}

if (!function_exists('prepare_params')) {
    /**
     * @param array $params
     *
     * @return void
     */
    function prepare_params(array &$params): void {
        foreach ($params as $key => &$param) {
            if (is_array($param)) {
                foreach ($param as $k => $p) {
                    prepare_param($param, $k);
                }
            } else {
                prepare_param($params, $key);
            }
        }
    }
}

if (!function_exists('prepare_array_param')) {
    /**
     * @param array $params
     * @param string $param
     *
     * @return array
     */
    function prepare_array_param(array &$params, string $param): array {
        /**
         * @var array $item
         */
        if (isset($params[$param])) {
            $item = $params[$param];
            unset($params[$param]);
        } else {
            $item = [];
        }

        return $item;
    }
}
