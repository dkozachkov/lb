<?php

return [
    'question_color' => env('DESIGN_QUESTION_COLOR', '#fff'),
    'answer_color' => env('DESIGN_ANSWER_COLOR', '#bcbcbc'),
    'button_color' => env('DESIGN_BUTTON_COLOR', '#c8e2f8'),
    'button_text' => env('DESIGN_BUTTON_TEXT', 'Пройти тест'),
    'text_color' => env('DESIGN_TEXT_COLOR', '#125a96'),
    'font_question_color' => env('DESIGN_FONT_QUESTION_COLOR', '#f2f2f2'),
    'font_answer_color' => env('DESIGN_FONT_ANSWER_COLOR', '#666'),
    'background_color' => env('DESIGN_BACKGROUND_COLOR', '#fff'),
];
