<?php

return [
    'integration_button_text' => env('INTEGRATION_BUTTON_TEXT', 'Чатбот'),
    'integration_button_color' => env('INTEGRATION_BUTTON_COLOR', '#f2f2f2'),
    'integration_button_text_color' => env('INTEGRATION_BUTTON_TEXT_COLOR', '#fff'),
];
