<?php

use App\Enums\Chatbot\ChatbotStatusEnum;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateChatbotsTable
 */
class CreateChatbotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {

            Schema::create('chatbots', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->bigInteger('user_id')->unsigned()->index();
                $table->string('uuid')->default('');
                $table->string('title')->default('');
                $table->string('sub_title')->default('');
                $table->string('avatar')->default('');
                $table->enum('status', ChatbotStatusEnum::values())->default(ChatbotStatusEnum::ACTIVATED());
                $table->string('favicon')->default('');

                $table->timestamps();
            });

            Schema::table('chatbots', function (Blueprint $table) {
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            });

        }, 3);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
        \DB::transaction(function () {

            Schema::table('chatbots', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropIndex(['user_id']);
            });

            Schema::dropIfExists('chatbots');

        }, 3);

    }
}
