<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateChatbotDesignsTable
 */
class CreateChatbotDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {

            Schema::create('chatbot_designs', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->bigInteger('chatbot_id')->unsigned()->index();
                $table->string('background')->default('');
                $table->string('question_color')->default('');
                $table->string('answer_color')->default('');
                $table->string('button_color')->default('');

                $table->timestamps();
            });

            Schema::table('chatbot_designs', function (Blueprint $table) {
                $table->foreign('chatbot_id')
                    ->references('id')
                    ->on('chatbots')
                    ->onDelete('cascade');
            });

        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
        \DB::transaction(function () {

            Schema::table('chatbot_designs', function (Blueprint $table) {
                $table->dropForeign(['chatbot_id']);
                $table->dropIndex(['chatbot_id']);
            });

            Schema::dropIfExists('chatbot_designs');

        }, 3);
    }

}
