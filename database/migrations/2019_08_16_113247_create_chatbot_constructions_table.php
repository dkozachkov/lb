<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateChatbotConstructionsTable
 */
class CreateChatbotConstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {

        \DB::transaction(function () {

            Schema::create('chatbot_constructions', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->bigInteger('chatbot_id')->unsigned()->index();
                $table->json('construction');
                $table->json('drafts');

                $table->timestamps();
            });

            Schema::table('chatbot_constructions', function (Blueprint $table) {
                $table->foreign('chatbot_id')
                    ->references('id')
                    ->on('chatbots')
                    ->onDelete('cascade');
            });

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
        \DB::transaction(function () {

            Schema::table('chatbot_constructions', function (Blueprint $table) {
                $table->dropForeign(['chatbot_id']);
                $table->dropIndex(['chatbot_id']);
            });

            Schema::dropIfExists('chatbot_constructions');

        });
    }
}
