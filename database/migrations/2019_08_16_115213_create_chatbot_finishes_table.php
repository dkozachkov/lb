<?php

use App\Enums\Chatbot\ChatbotFinish\ChatbotFinishMessageStatusEnum;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatbotFinishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {

            Schema::create('chatbot_finishes', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->bigInteger('chatbot_id')->unsigned()->index();

                $table->text('message');
                $table->enum('message_status', ChatbotFinishMessageStatusEnum::values())
                    ->default(ChatbotFinishMessageStatusEnum::ACTIVATED());

                $table->string('redirect_url')->default('');
                $table->enum('redirect_url_status', ChatbotFinishMessageStatusEnum::values())
                    ->default(ChatbotFinishMessageStatusEnum::ACTIVATED());

                $table->timestamps();
            });

            Schema::table('chatbot_finishes', function (Blueprint $table) {
                $table->foreign('chatbot_id')
                    ->references('id')
                    ->on('chatbots')
                    ->onDelete('cascade');
            });

        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
        \DB::transaction(function () {

            Schema::table('chatbot_finishes', function (Blueprint $table) {
                $table->dropForeign(['chatbot_id']);
                $table->dropIndex(['chatbot_id']);
            });

            Schema::dropIfExists('chatbot_finishes');

        }, 3);
    }
}
