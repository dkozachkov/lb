<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatbotSeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {

            Schema::create('chatbot_seo', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->bigInteger('chatbot_id')->unsigned()->index();
                $table->string('title')->default('');
                $table->text('description');
                $table->string('keywords')->default('');
                $table->string('policy_url')->default('');

                $table->timestamps();
            });

            Schema::table('chatbot_seo', function (Blueprint $table) {
                $table->foreign('chatbot_id')
                    ->references('id')
                    ->on('chatbots')
                    ->onDelete('cascade');
            });

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
        \DB::transaction(function () {

            Schema::table('chatbot_seo', function (Blueprint $table) {
                $table->dropForeign(['chatbot_id']);
                $table->dropIndex(['chatbot_id']);
            });

            Schema::dropIfExists('chatbot_seo');

        });
    }
}
