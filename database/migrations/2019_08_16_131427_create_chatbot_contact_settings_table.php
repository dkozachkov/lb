<?php

use App\Enums\Chatbot\ChatbotContactSetting\{
    ChatbotContactSettingEmailStatusEnum,
    ChatbotContactSettingNameStatusEnum,
    ChatbotContactSettingNeedEmailEnum,
    ChatbotContactSettingNeedNameEnum,
    ChatbotContactSettingNeedPhoneEnum,
    ChatbotContactSettingPhoneStatusEnum
};
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatbotContactSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {

        \DB::transaction(function () {

            Schema::create('chatbot_contact_settings', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->bigInteger('chatbot_id')->unsigned()->index();
                $table->enum('phone_status', ChatbotContactSettingPhoneStatusEnum::values())
                    ->default(ChatbotContactSettingPhoneStatusEnum::ACTIVATED());
                $table->enum('need_phone', ChatbotContactSettingNeedPhoneEnum::values())
                    ->default(ChatbotContactSettingPhoneStatusEnum::ACTIVATED());
                $table->string('phone_hints')->default('');
                $table->string('phone_mask')->default('');
                $table->enum('name_status', ChatbotContactSettingNameStatusEnum::values())
                    ->default(ChatbotContactSettingNameStatusEnum::ACTIVATED());
                $table->enum('need_name', ChatbotContactSettingNeedNameEnum::values())
                    ->default(ChatbotContactSettingNeedNameEnum::ACTIVATED());
                $table->string('name_hints')->default('');
                $table->enum('email_status', ChatbotContactSettingEmailStatusEnum::values())
                    ->default(ChatbotContactSettingEmailStatusEnum::ACTIVATED());
                $table->enum('need_email', ChatbotContactSettingNeedEmailEnum::values())
                    ->default(ChatbotContactSettingNeedEmailEnum::ACTIVATED());
                $table->string('email_hints')->default('');

                $table->timestamps();
            });

            Schema::table('chatbot_contact_settings', function (Blueprint $table) {
                $table->foreign('chatbot_id')
                    ->references('id')
                    ->on('chatbots')
                    ->onDelete('cascade');
            });

        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
        \DB::transaction(function () {

            Schema::table('chatbot_contact_settings', function (Blueprint $table) {
                $table->dropForeign(['chatbot_id']);
                $table->dropIndex(['chatbot_id']);
            });

            Schema::dropIfExists('chatbot_contact_settings');

        }, 3);
    }
}
