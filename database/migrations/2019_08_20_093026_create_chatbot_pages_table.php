<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatbotPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {

            Schema::create('chatbot_pages', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->bigInteger('chatbot_id')->unsigned()->index();
                $table->string('avatar')->default('');
                $table->string('background')->default('');
                $table->string('favicon')->default('');
                $table->string('title')->default('');
                $table->string('sub_title')->default('');

                $table->timestamps();
            });

            Schema::table('chatbot_pages', function (Blueprint $table) {
                $table->foreign('chatbot_id')
                    ->references('id')
                    ->on('chatbots')
                    ->onDelete('cascade');
            });

        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
        \DB::transaction(function () {

            Schema::table('chatbot_pages', function (Blueprint $table) {
                $table->dropForeign(['chatbot_id']);
                $table->dropIndex(['chatbot_id']);
            });

            Schema::dropIfExists('chatbot_pages');

        }, 3);
    }
}
