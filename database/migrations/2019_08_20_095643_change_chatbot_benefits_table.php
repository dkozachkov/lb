<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeChatbotBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {

            Schema::table('chatbot_benefits', function (Blueprint $table) {
                $table->dropForeign(['chatbot_id']);
                $table->dropIndex(['chatbot_id']);
                $table->dropColumn('chatbot_id');
            });

            Schema::table('chatbot_benefits', function (Blueprint $table) {
                $table->bigInteger('chatbot_page_id')
                    ->unsigned()
                    ->index();

                $table->foreign('chatbot_page_id')
                    ->references('id')
                    ->on('chatbot_pages')
                    ->onDelete('cascade');
            });

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chatbot_benefits', function (Blueprint $table) {
            //
        });
    }
}
