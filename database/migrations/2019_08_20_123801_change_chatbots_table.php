<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeChatbotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {

            Schema::table('chatbots', function (Blueprint $table) {
                $table->dropColumn(['sub_title', 'avatar', 'favicon']);
            });

            Schema::table('chatbot_designs', function (Blueprint $table) {
                $table->dropColumn(['background']);
            });

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chatbots', function (Blueprint $table) {
            //
        });
    }
}
