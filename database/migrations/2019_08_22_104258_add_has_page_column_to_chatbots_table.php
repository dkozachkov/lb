<?php

use App\Enums\Chatbot\ChatbotHasPageEnum;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasPageColumnToChatbotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chatbots', function (Blueprint $table) {
            $table->enum('has_page', ChatbotHasPageEnum::toArray())
                ->default(ChatbotHasPageEnum::DOESNT_HAS);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chatbots', function (Blueprint $table) {
            $table->dropColumn('has_page');
        });
    }
}
