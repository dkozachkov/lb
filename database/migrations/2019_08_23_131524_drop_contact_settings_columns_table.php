<?php

use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingNeedEmailEnum;
use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingNeedNameEnum;
use App\Enums\Chatbot\ChatbotContactSetting\ChatbotContactSettingNeedPhoneEnum;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropContactSettingsColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chatbot_contact_settings', function (Blueprint $table) {
            $table->dropColumn('need_phone');
            $table->dropColumn('need_name');
            $table->dropColumn('need_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chatbot_contact_settings', function (Blueprint $table) {
            $table->enum('need_phone', ChatbotContactSettingNeedPhoneEnum::values())
                ->default(ChatbotContactSettingNeedPhoneEnum::ACTIVATED());
            $table->enum('need_name', ChatbotContactSettingNeedNameEnum::values())
                ->default(ChatbotContactSettingNeedNameEnum::ACTIVATED());
            $table->enum('need_email', ChatbotContactSettingNeedEmailEnum::values())
                ->default(ChatbotContactSettingNeedEmailEnum::ACTIVATED());
        });
    }
}
