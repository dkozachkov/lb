<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFontColumnsToChatbotDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chatbot_designs', function (Blueprint $table) {
            $table->string('font_answer_color')->default('');
            $table->string('font_question_color')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chatbot_designs', function (Blueprint $table) {
            $table->dropColumn('font_answer_color');
            $table->dropColumn('font_question_color');
        });
    }
}
