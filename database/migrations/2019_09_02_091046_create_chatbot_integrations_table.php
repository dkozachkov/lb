<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatbotIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::create('chatbot_integrations', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->unsignedBigInteger('chatbot_id')->index();

                $table->string('button_color')->default('');
                $table->string('text_color')->default('');
                $table->string('button_text')->default('');

                $table->timestamps();
            });

            Schema::table('chatbot_integrations', function (Blueprint $table) {
                $table->foreign('chatbot_id')
                    ->references('id')
                    ->on('chatbots')
                    ->onDelete('cascade');
            });
        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
       \DB::transaction(function () {
           Schema::table('chatbot_integrations', function (Blueprint $table) {
               $table->dropForeign(['chatbot_id']);;
               $table->dropIndex(['chatbot_id']);;
           });

           Schema::dropIfExists('chatbot_integrations');
       }, 3);
    }
}
