<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMessagesLenghtInChatbotFinishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::table('chatbot_finishes', function (Blueprint $table) {
                $table->dropColumn('sub_message');
            });

            Schema::table('chatbot_finishes', function (Blueprint $table) {
                $table->text('sub_message');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function down()
    {
        \DB::transaction(function () {
            Schema::table('chatbot_finishes', function (Blueprint $table) {
                $table->dropColumn('sub_message');
            });

            Schema::table('chatbot_finishes', function (Blueprint $table) {
                $table->string('sub_message')
                    ->nullable();
            });
        });
    }
}
