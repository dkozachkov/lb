<?php

use App\Enums\Integration\IntegrationEmailStatusEnum;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::create('service_integrations', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->unsignedBigInteger('user_id')->index();
                $table->string('email_theme');
                $table->enum('email_status', IntegrationEmailStatusEnum::values())
                    ->default(IntegrationEmailStatusEnum::ACTIVATED);
            });

            Schema::table('service_integrations', function (Blueprint $table) {
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            });
        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            Schema::table('service_integrations', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropIndex(['user_id']);
            });
            Schema::dropIfExists('service_integrations');
        }, 3);
    }
}
