<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::create('email_integrations', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('service_integration_id')->index();
                $table->string('email');
                $table->timestamps();
            });

            Schema::table('email_integrations', function (Blueprint $table) {
                $table->foreign('service_integration_id')
                    ->references('id')
                    ->on('service_integrations')
                    ->onDelete('cascade');
            });
        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            Schema::table('email_integrations', function (Blueprint $table) {
                $table->dropForeign(['service_integration_id']);
                $table->dropIndex(['service_integration_id']);
            });
            Schema::dropIfExists('email_integrations');
        }, 3);
    }
}
