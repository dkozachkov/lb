<?php

use App\Enums\Chatbot\ChatbotIntegrationEmailStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatbotServiceIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::create('chatbot_service_integrations', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->unsignedBigInteger('chatbot_id')->index();
                $table->string('email_theme');
                $table->enum('email_status', ChatbotIntegrationEmailStatusEnum::values())
                    ->default(ChatbotIntegrationEmailStatusEnum::ACTIVATED);
            });

            Schema::table('chatbot_service_integrations', function (Blueprint $table) {
                $table->foreign('chatbot_id')
                    ->references('id')
                    ->on('chatbots')
                    ->onDelete('cascade');
            });
        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            Schema::table('chatbot_service_integrations', function (Blueprint $table) {
                $table->dropForeign(['chatbot_id']);
                $table->dropIndex(['chatbot_id']);
            });
            Schema::dropIfExists('chatbot_service_integrations');
        }, 3);
    }
}
