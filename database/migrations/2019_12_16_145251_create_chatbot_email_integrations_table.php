<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatbotEmailIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::create('chatbot_email_integrations', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('chatbot_service_id')->index();
                $table->string('email');
                $table->timestamps();
            });

            Schema::table('chatbot_email_integrations', function (Blueprint $table) {
                $table->foreign('chatbot_service_id')
                    ->references('id')
                    ->on('chatbot_service_integrations')
                    ->onDelete('cascade');
            });
        }, 3);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            Schema::table('chatbot_email_integrations', function (Blueprint $table) {
                $table->dropForeign(['chatbot_service_id']);
                $table->dropIndex(['chatbot_service_id']);
            });
            Schema::dropIfExists('chatbot_email_integrations');
        }, 3);
    }
}
