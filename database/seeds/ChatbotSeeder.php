<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class ChatbotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var \App\Models\User $user
         */
        $user = User::firstOrFail();

        /**
         * @var \App\Models\Chatbot $chatbot
         */
        $chatbot = $user->chatbots()->create([
            'title' => 'Test chatbot',
            'domain' => '',
        ]);

        for ($i = 0; $i < 4; $i++) {
            $chatbot->request()->create([
                'phone' => "+{$i}80{$i}55{$i}5{$i}54{$i}",
                'name' => "Name of an user $i",
                'email' => "{$i}email@mail.com",
            ]);
        }
    }
}
