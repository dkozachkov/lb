<?php

use App\Models\IconCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class IconsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hasCategories = IconCategory::count() > 0;

        if (!$hasCategories) {
            $resourcesStorage = Storage::disk('resources');
            $resourcePath = $resourcesStorage->path('icons');
            $publicStorage = Storage::disk('public');
            $publicPath = $publicStorage->path('icons');

            \File::copyDirectory($resourcePath, $publicPath);

            $categories = Storage::disk('public')->directories('icons');
            foreach ($categories as &$category) {
                $category = str_replace('icons/', '', $category);
            }
            unset($category);

            foreach ($categories as $category) {
                IconCategory::create([
                    'title' => $category,
                    'path' => "icons/$category",
                ]);
            }
        }
    }

}
