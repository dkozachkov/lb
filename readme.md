# How to deploy the project

You must clone the project from a repository. For clone the project enter the next command:
> git clone git@bitbucket.org:stamax-dev/leedbot.git

> git checkout develop

After cloning the project you must run the command:
> composer install

Rename ".env.example" file to ".env" file.
> cp .env.example .env

Generate a app key
> php artisan key:generate 

Now you must run the next command:
> php artisan passport:install

Ok. The backend of the project is configured. Now you must configure the frontend.

Run the next command:
> npm i

And a command:
> npm run prod
