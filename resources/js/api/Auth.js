export default class Auth {
    static login(data) {
        return axios.post('/login', data);
    }

    static registration(data) {
        return axios.post('/register', data);
    }

    static accessRestore(data) {
        return axios.post('/api/password/email', data);
    }
}
