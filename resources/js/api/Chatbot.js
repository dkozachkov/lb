export default class Chatbot {
    static getChatbot(chatbot_id) {
        return axios.get(`/api/chatbots/${chatbot_id}/construction`);
    }
    static updateModel(chatbot_id, data) {
        return axios.put(`/api/chatbots/${chatbot_id}/construction`, data);
    }
    static show(chatbot_id) {
        return axios.get(`/api/chatbots/${chatbot_id}`);
    }
    static store(data) {
        return axios.post(`/api/chatbots`, data);
    }
    static update(chatbot_id, data) {
        return axios.post(`/api/chatbots/${chatbot_id}`, data);
    }
    static duplicate(chatbot_id) {
        return axios.post(`/api/chatbots/${chatbot_id}/duplicate`);
    }
    static delete(chatbot_id) {
        return axios.delete(`/api/chatbots/${chatbot_id}`);
    }
    static getSettings(chatbotId) {
        return axios.get(`/api/chatbots/${chatbotId}/settings`);
    }
    static updateSettings(chatbot_id, data) {
        return axios.post(`/api/chatbots/${chatbot_id}/settings`, data);
    }
    static toggleStatus(chatbot_id) {
        return axios.patch(`/api/chatbots/${chatbot_id}/toggle-status`);
    }
    static resetDesign(chatbot_id) {
        return axios.get(`/api/chatbots/${chatbot_id}/settings/reset`);
    }
}
