export default class Chatbots {
    static get(params) {
        return axios.get(`/api/chatbots`, { params });
    }
}
