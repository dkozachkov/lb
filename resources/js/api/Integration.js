export default class Integration {
    static get(chatbotId) {
        return axios.get(`/api/chatbots/${chatbotId}/integration`);
    }

    static updateButton(chatbot_id, data) {
        return axios.put(`/api/chatbots/${chatbot_id}/integration`, data);
    }
}
