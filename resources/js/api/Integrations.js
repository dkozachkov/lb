export default class Integrations {
    static getEmails(chatbotId = null) {
        const isChatBot = chatbotId ? `/${chatbotId}` : '';
        return axios.get(`/api/integrations/emails${isChatBot}`);
    }
    static updateEmails(data, chatbotId = null) {
        const isChatBot = chatbotId ? `/${chatbotId}` : '';
        return axios.put(`/api/integrations/emails${isChatBot}`, data);
    }
    static toggleStatusEmail(data, chatbotId = null) {
        const isChatBot = chatbotId ? `/${chatbotId}` : '';
        return axios.patch(`/api/integrations/emails${isChatBot}`, data);
    }
}
