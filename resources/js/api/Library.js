export default class Library {
    static getIconsCategory() {
        return axios.get(`/api/icons/categories`);
    }

    static getIcons(categoryId) {
        return axios.get(`/api/icons/${categoryId}`);
    }
}
