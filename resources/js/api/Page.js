export default class Page {
    constructor() {
        this.config = { headers: { 'Content-Type': 'multipart/form-data' } };
    }

    static get(chatbotId) {
        return axios.get(`/api/chatbots/${chatbotId}/page`);
    }

    static store(data, chatbotId) {
        return axios.post(`/api/chatbots/${chatbotId}/page`, data, this.config);
    }

    static update(data, chatbotId) {
        return axios.post(`/api/chatbots/${chatbotId}/page/update`, data, this.config);
    }
}
