export default class Requests {
    static get(id, params) {
        return axios.get(`/api/chatbots/${id}/requests`, { params });
    }
    static delete(chatbotId, requestId) {
        return axios.delete(`/api/chatbots/${chatbotId}/requests/${requestId}`);
    }
    static export(chatbotId) {
        return axios.get(`/api/chatbots/${chatbotId}/requests/export`, {
            responseType: 'arraybuffer',
            headers: {
                Accept: 'application/xls'
            }
        });
    }
}
