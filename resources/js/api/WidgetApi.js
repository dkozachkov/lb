import axios from 'axios';
const $axios = axios.create({
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
    },
    baseURL: process.env.MIX_APP_URL,
});

export default class WidgetApi {
    static get(id) {
        return $axios.get(`api/chatbot/${id}`);
    }
    static store(id, data) {
        return $axios.post(`api/chatbot/${id}`, data);
    }
}
