export default {
    // background: '#fbdd2d',
    confirmButtonText: 'Ок',
    cancelButtonText: 'Отмена',
    confirmButtonColor: '#3f9ce8',
    cancelButtonColor: '#000000',
    customClass: {
        popup: 'swal__content',
        input: 'field-modal-item',
        confirmButton: 'swal__btn swal__btn--confirm',
        cancelButton: 'swal__btn'
    }
};
