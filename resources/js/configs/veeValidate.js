import en from 'vee-validate/dist/locale/en';
import ru from 'vee-validate/dist/locale/ru';
export default {
    locale: 'ru',
    dictionary: {
        en: en,
        ru: {
            messages: {
                ...ru.messages,
                required: () => 'Поле обязательно для заполнения',
                min: (fieldName, params, data) => `В поле должно быть минимум ${params} символов`
            }
        }
    },
    events: 'input|blur|on-close'
};
