/**
 * @param {Array} array1 - first array
 * @param {Array} array2 - second array
 */
const _equalArrays = (array1, array2) => {
    if (!Array.isArray(array1) || !Array.isArray(array2)) {
        throw 'Invalid arguments, is not array';
    }
    if (array1.length !== array2.length) {
        return false;
    }
    const results = array1.filter(el => array2.includes(el));

    return results.length == array1.length;
};
const _containsArrays = (array1, array2) => {
    if (!Array.isArray(array1) || !Array.isArray(array2)) {
        throw 'Invalid arguments, is not array';
    }

    const results = array1.filter(el => array2.includes(el));

    return results.length;
};

export { _equalArrays, _containsArrays };
