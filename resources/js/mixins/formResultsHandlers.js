const formResultsHandlers = {
    methods: {
        successSend(data) {
            this.isLoading = false;
            Swal.fire({
                ...SwalConfig,
                title: 'Данные успешно сохранены',
                type: 'success',
                showCancelButton: false
            });
        },
        failSend(respond, scope) {
            this.isLoading = false;
            if (respond.status == 422) {
                for (const key in respond.data.errors) {
                    this.errors.add({
                        field: key,
                        scope: scope,
                        msg: respond.data.errors[key][0]
                    });
                    const field = this.$validator.fields.find({
                        name: key,
                        scope: scope
                    });
                    field.setFlags({
                        valid: false,
                        dirty: true
                    });
                }
            } else {
                Swal.fire({
                    ...SwalConfig,
                    title: respond.data.errors,
                    type: 'error',
                    showCancelButton: false
                });
            }
        }
    }
};
export default formResultsHandlers;
