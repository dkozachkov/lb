const listAnimation = {
    methods: {
        beforeEnter(el) {
            $(el).animate(
                {
                    opacity: 0,
                    height: 'toggle'
                },
                0
            );
        },
        enter(el, done) {
            const _this = this;
            $(el)
                .delay(1000)
                .animate(
                    {
                        opacity: 1,
                        height: 'toggle'
                    },
                    {
                        duration: 1200,
                        step: function(now, fx) {
                            _this.simplebar.scrollContentEl.scrollTop = _this.simplebarBox.scrollHeight + 100;
                        },
                        start() {
                            _this.isTyping = false;
                        },
                        complete() {
                            done();
                        }
                    }
                );
        },
        leave(el, done) {
            el.style.display = 'none';
            done();
        }
    }
};
export default listAnimation;
