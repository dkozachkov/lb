const CONDITION_TYPES = {
    numbers: {
        under: 'Если менее',
        equally: 'Если равно',
        more: 'Если более'
    },
    text: {
        contains: 'Если содержит',
        notContains: 'Если не содержит'
    }
};

export default CONDITION_TYPES;
