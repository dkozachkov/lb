import NODE_COLORS from './nodeColors';
const CONFIG = {
    style: [
        {
            selector: 'node',
            style: {
                'background-color': NODE_COLORS.default,
                label: 'data(title)',
                'font-size': 10,
                'font-family': 'Helvetica Neue',
                color: '#125a96',
                // width: 'label',
                width: 120,
                height: 40,
                'text-wrap': 'ellipsis',
                'text-max-width': 100,
                'text-valign': 'center',
                'text-halign': 'center',
                shape: 'round-rectangle'
                // padding: 5
            }
        },
        {
            selector: 'edge',
            style: {
                content: 'data(title)',
                width: 1,
                'line-color': '#ccc',
                'text-valign': 'top',
                'font-size': 10,
                color: '#717171',
                'text-margin-y': '-10px',
                'text-margin-x': '-10px',
                'target-arrow-color': '#ccc',
                'target-arrow-shape': 'triangle',
                'curve-style': 'taxi',
                'edge-text-rotation': 'autorotate'
            }
        }
    ],
    layout: {
        name: 'breadthfirst',
        animate: true,
        transform: (node, position) => {
            if (node.position()) return node.position();
            return position;
        },
        fit: false
    }
};
export default CONFIG;
