const ICONS = {
    info: 'si-info',
    question: 'si-question',
    entry: 'si-pencil',
    date: 'si-calendar',
    time: 'si-clock',
    slider: 'si-equalizer',
    droplist: 'si-list'
};
const LABEL_CONFIG = [
    {
        query: '.l1',
        // valign: 'center',
        // halign: 'center',
        // valignBox: 'center',
        // halignBox: 'center',
        cssClass: 'node__item',
        // width: 100,
        tpl: function(data) {
            return `
                <div class="text">${data.title}</div>
                <i class="si ${ICONS[data.type]} item__icon" aria-hidden="true"></i>
                <i class="si si-plus item__icon--fly js-open-node-modal ${data.hasChild || !data.hasOwnProperty('hasChild') ? 'hidden' : ''}" data-source="${data.id}" aria-hidden="true"></i>
            `;
        }
    }
    // {
    //     query: '.l2',
    //     valign: 'bottom',
    //     halign: 'center',
    //     valignBox: 'top',
    //     halignBox: 'left',
    //     cssClass: 'l1',
    //     tpl: function(data) {
    //         return `<i class="si si-plus item__icon--fly" aria-hidden="true"></i>`;
    //     }
    // }
];
export { LABEL_CONFIG, ICONS };
