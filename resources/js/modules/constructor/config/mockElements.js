const MOCK_ELEMENTS = [
    {
        data: {
            id: '1567584491302',
            title: 'Привет',
            answers: [],
            type: 1,
            multipleChoice: false
        },
        classes: 'l1',
        position: { x: -1.789551907790524, y: -102.00445874405987 }
    },
    {
        data: {
            id: '1567584541928',
            title: 'Как тебя зовут?',
            answers: ['Сергей'],
            type: 3,
            multipleChoice: false
        },
        classes: 'l1',
        position: { x: 127.57778682703471, y: -49.98687994761588 }
    },
    {
        data: {
            id: '1567584575941',
            title: 'В каком городе ты живешь?',
            answers: ['Одесса', 'Киев', 'Львов'],
            type: 2,
            multipleChoice: false
        },
        classes: 'l1',
        position: { x: 302.8943424854216, y: 99.10145503072772 }
    },
    {
        data: {
            id: '1567584605373',
            title: 'Отлично, мы с тобой свяжемся',
            answers: [],
            type: 1,
            multipleChoice: false
        },
        classes: 'l1',
        position: { x: 14.039891906404907, y: 275.3039673821136 }
    },
    {
        data: {
            id: '1567584546808',
            title: null,
            source: '1567584491302',
            target: '1567584541928'
        }
    },
    {
        data: {
            id: '1567584584765',
            title: 'Сергей',
            source: '1567584541928',
            target: '1567584575941'
        }
    },
    {
        data: {
            id: '1567584615010',
            title: 'Одесса',
            source: '1567584575941',
            target: '1567584605373'
        }
    }
];

export default MOCK_ELEMENTS;
