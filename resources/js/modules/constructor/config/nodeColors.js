const NODE_COLORS = {
    default: '#c8e2f8',
    active: '#9ac5ea',
    selectFirst: '#fcf7e6',
    selectSecond: '#ebf5df',
    newNode: '#fae9e8'
};
export default NODE_COLORS;
