const TYPE_QUESTION = {
    info: 'Информационный',
    question: 'Вопрос-ответ',
    entry: 'Свободный ввод',
    date: 'Дата',
    // time: 'Время',
    slider: 'Ползунок',
    droplist: 'Выбор списка (дроплист)'
};

export default TYPE_QUESTION;
