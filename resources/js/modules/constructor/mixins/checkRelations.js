import { _equalArrays, _containsArrays } from '@/js/helpers/equalArrays';
const checkRelationMixin = {
    methods: {
        checkDuplicate(elements, newElement) {
            //Check for duplicate relations
            let result = false;
            for (let index = 0; index < elements.length; index++) {
                const el = elements[index].data;
                const title = Array.isArray(el.title) ? el.title : [el.title];
                const newTitle = Array.isArray(newElement.title) ? newElement.title : [newElement.title];
                const condition = el.hasOwnProperty('condition', 'type') ? el.condition.type : '';
                const newCondition = newElement.hasOwnProperty('condition', 'type') ? newElement.condition.type : '';
                const source = el.source;
                const newSource = newElement.source;

                if (source == newSource && _containsArrays(title, newTitle) && condition == newCondition) {
                    result = true;
                    break;
                }
            }

            return result;
        },
        checkMultiRelation(elements, newElement) {
            //Check for more than one relationship between two nodes
            let result = false;
            elements.forEach(el => {
                const newSource = newElement.source;
                const newTarget = newElement.target;
                const resultArray = this.elements.filter(el => (el.data.target == newTarget && el.data.source == newSource) || (el.data.target == newSource && el.data.source == newTarget));
                if (resultArray.length) result = true;
            });
            return result;
        }
    }
};

export default checkRelationMixin;
