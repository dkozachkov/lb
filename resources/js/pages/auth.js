import Vue from 'vue';
import VeeValidate from 'vee-validate';
import veeValidateConfig from '@/js/configs/veeValidate';
import Auth from '@/js/api/Auth';

Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    name: 'Auth',
    data: {
        form: {
            email: '',
            password: '',
            _token
        },
        serverError: '',
        isLoading: false
    },
    el: '#v-auth',
    methods: {
        async sendForm() {
            const valid = await this.$validator.validate('auth.*');
            if (!valid) return false;
            this.isLoading = true;
            Auth.login(this.form)
                .then(res => this.successSend(res))
                .catch(errors => this.failSend(errors.response));
        },
        successSend(data) {
            this.isLoading = false;
            this.$nextTick(() => {
                this.resetForm('#v-auth-form');
            });
            window.location.href = window.location.origin;
        },
        failSend(errors) {
            this.isLoading = false;
            console.log('data', errors);
            if (errors.status == 422) {
                this.addedErrorForField('email', 'auth', 'Введенные логин или пароль не верные');
                this.addedErrorForField('password', 'auth', 'Введенные логин или пароль не верные');
            } else {
                this.serverError = errors.data.errors;
            }
        },
        addedErrorForField(name, scope, msg) {
            this.errors.add({
                field: name,
                scope: scope,
                msg: msg
            });
            const field = this.$validator.fields.find({ name: name });
            field.setFlags({
                valid: false,
                dirty: true
            });
        },
        resetForm(formId) {
            $(formId).trigger('reset');
            this.$validator.reset();
        }
    }
});
