import Vue from 'vue';
import VeeValidate from 'vee-validate';
import veeValidateConfig from '@/js/configs/veeValidate';
import constructorPage from '@/js/modules/constructor';
// import { Datetime } from 'vue-datetime';
// import Datetime from 'vue-datetime';
// You need a specific loader for CSS files
// import 'vue-datetime/dist/vue-datetime.css';
// import { Datetime } from 'vue-datetime';
// window.Vue = Vue;
// Vue.component('datetime', Datetime);
// Vue.use(Datetime);
// Vue.component('datetime', Datetime);
Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    name: 'ConstructorPage',
    components: {
        constructorPage
    },
    render: h => h(constructorPage),
    el: '#v-constructor'
});
