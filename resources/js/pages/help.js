import Vue from "vue";
import helpPage from "@/js/modules/help";

new Vue({
    name: "HelpPage",
    render: h => h(helpPage),
    el: "#v-help"
});
