import Vue from 'vue';
import VeeValidate from 'vee-validate';
import veeValidateConfig from '@/js/configs/veeValidate';
import chatbotsPage from '@/js/modules/chatbots';

Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    name: 'ChatbotsPage',
    components: {
        chatbotsPage
    },
    render: h => h(chatbotsPage),
    el: '#v-main'
});
