import Vue from 'vue';
import VeeValidate from 'vee-validate';
import veeValidateConfig from '@/js/configs/veeValidate';
import integrationsPage from '@/js/modules/integrations';

Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    el: '#v-integrations',
    name: 'IntegrationsPage',
    render: h => h(integrationsPage)
});
