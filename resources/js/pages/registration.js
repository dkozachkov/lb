import Vue from 'vue';
import VeeValidate from 'vee-validate';
import veeValidateConfig from '@/js/configs/veeValidate';
import Auth from '@/js/api/Auth';

Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    data() {
        return {
            form: {
                name: '',
                email: '',
                password: '',
                password_confirmation: ''
            },
            serverError: '',
            isLoading: false
        };
    },
    el: '#v-registration',
    methods: {
        async sendForm() {
            const valid = await this.$validator.validate('registration.*');
            if (!valid) return false;
            this.isLoading = true;
            Auth.registration(this.form)
                .then(res => this.successSend(res))
                .catch(errors => this.failSend(errors.response));
        },

        successSend(data) {
            this.isLoading = false;
            window.location.href = window.location.origin;
        },
        failSend(errors) {
            this.isLoading = false;
            console.log('data', errors);
            if (errors.status == 422) {
                for (const key in errors.data.errors) {
                    this.errors.add({
                        field: key,
                        scope: 'registration',
                        msg: errors.data.errors[key][0]
                    });
                    const field = this.$validator.fields.find({ name: key });
                    field.setFlags({
                        valid: false,
                        dirty: true
                    });
                }
            } else {
                this.serverError = errors.data.errors;
            }
        },
        resetForm(formId) {
            $(formId).trigger('reset');
            this.$validator.reset();
        }
    }
});
