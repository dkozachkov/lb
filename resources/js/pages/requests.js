import Vue from 'vue';
import requestsPage from '@/js/modules/requests';

new Vue({
    name: 'RequestsPage',
    components: {
        requestsPage
    },
    render: h => h(requestsPage),
    el: '#v-requests'
});
