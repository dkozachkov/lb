import Vue from 'vue';
import VeeValidate from 'vee-validate';
import veeValidateConfig from '@/js/configs/veeValidate';
import settingsPage from '@/js/modules/settings';

Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    name: 'SettingsPage',
    components: {
        settingsPage
    },
    render: h => h(settingsPage),
    el: '#v-settings'
});
