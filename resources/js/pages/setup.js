import Vue from "vue";
import VeeValidate from "vee-validate";
import veeValidateConfig from "@/js/configs/veeValidate";
import setupPage from "@/js/modules/setup";

Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    name: "SetupPage",
    render: h => h(setupPage),
    el: "#v-setup"
});
