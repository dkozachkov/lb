import Vue from 'vue';
import VeeValidate from 'vee-validate';
import veeValidateConfig from '@/js/configs/veeValidate';
import templatePage from '@/js/modules/template';
import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use( CKEditor );
Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    name: 'TemplatePage',
    components: {
        templatePage
    },
    render: h => h(templatePage),
    el: '#v-template'
});
