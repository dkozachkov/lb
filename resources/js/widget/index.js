import './style.scss';
/**
 * @param  {} id - uuid chatbot
 * @param  {} type='default' - view for render function
 * @param  {} text='' - if type button - button text
 * @param  {} color='' - if type button - button color
 * @param  {} background='' - if type button - button background
 * @param  {} selector='.leedbot-init' - if type default - render selector
 */
class LeedBot {
    constructor(config) {
        this.chatBotIsOpen = false;
        this.config = config;

        if (document.readyState === 'complete') {
            this.init();
        } else {
            window.addEventListener('load', () => this.init(), false);
        }
    }
    startDialog (chatBot) {
        chatBot.postMessage('start', '*');
    }
    toggleWidget (button, iframe) {
        iframe.style.transform = `translateX(${
            this.chatBotIsOpen ? '400px' : '0'
            })`;
        button.classList.toggle('is-open');
        this.chatBotIsOpen = !this.chatBotIsOpen;
    }
    createButton () {
        const { id, background, color, text } = this.config;
        const button = document.createElement('div');
        const template = `
                <button
                    class="v-leedbot_ru__widget__button"
                    id="v-button_${id}"
                    style="background: ${background}; color: ${color}; position: fixed; bottom: 30px; right: 30px; z-index: 99999;">
                    <svg version="1.1"
                        viewBox="0 0 31.112 31.112"
                        style="enable-background:new 0 0 31.112 31.112;"
                        xml:space="preserve">
                        <polygon points="31.112,1.414 29.698,0 15.556,14.142 1.414,0 0,1.414 14.142,15.556 0,29.698 1.414,31.112 15.556,16.97
                    29.698,31.112 31.112,29.698 16.97,15.556 " />
                        </svg>
                        <span class="v-leedbot_ru__widget__text">${text}</span>
                </button>`;
        button.innerHTML = template;
        document.body.appendChild(button);

        return document.getElementById(`v-button_${id}`);
    }
    createIframe () {
        const { id, type } = this.config;
        let iframe = document.createElement('iframe');
        iframe.setAttribute('role', 'presentation');
        iframe.setAttribute('id', `v-iframe_${id}`);
        iframe.setAttribute('name', `v-bot_${id}`);
        iframe.setAttribute('tabindex', '2');
        iframe.setAttribute('aria-hidden', 'true');
        iframe.frameBorder = 0;
        iframe.src = `${process.env.MIX_APP_URL}/widgets?id=${id}`;
        if (type == 'button') {
            iframe.style.position = 'fixed';
            iframe.style.width = '420px';
            iframe.style.maxWidth = '100%';
            iframe.style.height = '580px';
            iframe.style.maxHeight = 'calc(100vh - 60px)';
            iframe.style.bottom = '70px';
            iframe.style.right = '0';
            iframe.style.transition = '0.5s';
            iframe.style.zIndex = '999999';
            iframe.style.transform = 'translateX(400px)';
            document.body.appendChild(iframe);
        } else {
            const block = document.getElementById(`v-leedbot_${id}`);
            block.style.height = '0';
            iframe.style.width = '100%';
            iframe.style.height = '100%';
            block.appendChild(iframe);
        }
        return iframe;
    }
    showBlock (chatBot) {
        const { id } = this.config;
        const block = document.getElementById(`v-leedbot_${id}`);
        block.style.height = '70vh';

    }
    showButton (iframe, chatBot) {
        const button = this.createButton();
        const toggle = () => this.toggleWidget(button, iframe);
        const start = () => this.startDialog(chatBot);
        button.addEventListener('click', toggle);
        button.addEventListener('click', start, {
            once: true
        });
    }
    observerEventStart (chatBot, id) {
        const block = document.getElementById(`v-leedbot_${id}`);
        const options = {
            threshold: 0.5
        };
        const callback = (entries, observer) => {
            if (entries[0].intersectionRatio > 0.5) {
                this.startDialog(chatBot);
                observer.disconnect()
            }
        };
        const observer = new IntersectionObserver(callback, options);
        observer.observe(block);
    }
    init () {
        const { id, type } = this.config;
        const iframe = this.createIframe();

        const onloadIframe = () => {
            const chatBot = window.frames[`v-bot_${id}`];
            const configStr = JSON.stringify({ ...this.config, init: true });
            chatBot.postMessage(configStr, '*');
            const listenerWidget = ({ data }) => {
                if (data === `${id}_is_ready`) {
                    if (type == 'button') {
                        this.showButton(iframe, chatBot);
                    } else {
                        console.log('render');
                        this.showBlock(chatBot);
                        this.observerEventStart(chatBot, id);
                    }
                }
            };
            window.addEventListener('message', listenerWidget, false); // wait for get chatbot data in iframe
        };

        iframe.onload = onloadIframe;
    }
}
window.LeedBot = LeedBot;
