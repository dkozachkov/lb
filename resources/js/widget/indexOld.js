// import axios from 'axios';
// const $axios = axios.create({
//     headers: {
//         'Access-Control-Allow-Origin': '*',
//         'Content-Type': 'application/json'
//     }
// });

import Vue from 'vue';
import Chat from '@/js/modules/chat';
import WidgetApi from '@/js/api/WidgetApi';
/**
 * @param  {} id - uuid chatbot
 * @param  {} type='default' - view for render function
 * @param  {} text='' - if type button - button text
 * @param  {} color='' - if type button - button color
 * @param  {} background='' - if type button - button background
 * @param  {} selector='.leedbot-init' - if type default - render selector
 */
class LeedBot {
    constructor({ id, type = 'default', text = '', color = '', background = '', selector }) {
        console.log({ id, type, text, color, background, selector });
        window.leedBotConfig = { id, type, text, color, background, selector };
        if (type == 'button') document.body.innerHTML += `<div id="v-leedbot_${id}"></div>`;
        this.getInitData();
    }
    async getInitData() {
        try {
            const { data } = await WidgetApi.get(window.leedBotConfig.id);
            this.init(data);
        } catch (error) {
            throw `Load data error: ${error}`;
        }
    }

    init(data) {
        new Vue({
            components: { Chat },
            data: {
                chatbotData: data
            },
            render: h =>
                h(Chat, {
                    props: {
                        model: JSON.parse(data.construction.construction),
                        config: data,
                        typeRender: window.leedBotConfig.type,
                        buttonSettings: window.leedBotConfig
                    }
                }),
            el: `${window.leedBotConfig.selector}`
        });
    }
}

window.LeedBot = LeedBot;
