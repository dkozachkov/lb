import Vue from "vue";
import Chat from "@/js/modules/chat";
import WidgetApi from "@/js/api/WidgetApi";

class Widget {
    constructor(data) {
        let url = new URL(window.location.href);
        let id = url.searchParams.get("id");
        this.config = data;
        this.getInitData(id);
    }

    async getInitData(id) {
        try {
            const { data } = await WidgetApi.get(id);
            this.init(data);
            window.top.postMessage(`${id}_is_ready`, "*");
        } catch (error) {
            throw `Load data error: ${error}`;
        }
    }

    init(data) {
        new Vue({
            data: {
                chatbotData: data
            },
            render: h =>
                h(Chat, {
                    props: {
                        model: JSON.parse(data.construction.construction),
                        config: data,
                        typeRender: this.config.type,
                        buttonSettings: this.config
                    }
                }),
            el: "#app"
        });
    }
}
const initialize = ({ data }) => {
    if (!data || data == "start") return;
    try {
        const config = JSON.parse(data);
        if (config.init == true) {
            new Widget(config);
        }
    } catch (error) {
        console.log("chat-bot initialize error", error);
    }
};

window.addEventListener("message", initialize, false);
