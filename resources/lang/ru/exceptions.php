<?php

return [
    'server_error' => 'Что-то пошло не так.',
    'not_found' => 'Ресурс не был найден',
];
