@extends('layouts.auth')

@section('content')
<!-- START Sign In Form -->
<div id="v-auth">
    <form class="px-30" @submit.prevent="sendForm" data-vv-scope="auth" id='v-auth-form'>
        <h2 class="h5 font-w400 text-muted mb-20">Вход</h2>
        <div class="form-group row">
            <div class="col-12">
                <div class="form-material " :class="{'is-invalid': errors.has('auth.email')}">
                    <input type="text" class="form-control" class="field-modal-item" name="email" v-model="form.email"
                        v-validate="'required|email'">
                    <label for="login-username">Ваш E-mail</label>
                    <div class="invalid-feedback animated fadeInDown">
                        @{{ errors.first('auth.email') }}</div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="form-material " :class="{'is-invalid': errors.has('auth.password')}">
                    <input type="password" class="form-control" autocomplete="off" v-model="form.password"
                        ref="password" v-validate="'required|min:8'" name="password">
                    <label for="login-password">Ваш пароль</label>
                    <div class="invalid-feedback animated fadeInDown">
                        @{{ errors.first('auth.password') }}</div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-sm btn-hero btn-alt-primary " :class="{'loading': isLoading}"
                :disabled="isLoading">
                <i class="si si-login mr-10"></i> Войти
            </button>
            <div class="mt-30">
                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('user.register')}}">
                    <i class="fa fa-plus mr-5"></i> Регистрация
                </a>
                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('password.request')}}">
                    <i class="fa fa-refresh mr-5"></i> Восстановить пароль
                </a>
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-12 offset-md-1">
                <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-primary"><i class="fa fa-facebook"></i>
                    Facebook</a>
                <a href="{{ url('/auth/redirect/google') }}" class="btn btn-danger"><i class="fa fa-google"></i>
                    Google</a>
                <a href="{{ url('/auth/redirect/vkontakte') }}" class="btn btn-primary"><i class="fa fa-vk"></i> VK</a>
            </div>
        </div>
    </form>
</div>
<!-- END Sign In Form -->
@endsection

@push('scripts')
<script src="{{mix('js/pages/auth.js')}}"></script>
@endpush
