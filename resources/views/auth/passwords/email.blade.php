@extends('layouts.auth')

@section('content')
<!-- START Sign In Form -->
<form method="POST" action="{{ route('password.email') }}" class="px-30">
    @csrf
    <h2 class="h5 font-w400 text-muted mb-20">Восстановление пароля</h2>
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    <div class="form-group">
        <div class="mt-30">
            <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('login-page')}}">
                <i class="fa fa-plus mr-5"></i> Вход
            </a>
            <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('user.register')}}">
                <i class="fa fa-plus mr-5"></i> Регистрация
            </a>
        </div>
    </div>
    @else
    <div class="form-group row">
        <div class="col-12">
            <div class="form-material @error('email') is-invalid @enderror"" >
                        <input type=" email" class="form-control" name="email" value="{{ old('email') }}" required
                autocomplete="email" autofocus>
                <label for="login-username">Ваш E-mail</label>
                @error('email')
                <div class="invalid-feedback animated fadeInDown">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-sm btn-hero btn-alt-primary" :disabled="isLoading">
            <i class="si si-paper-plane mr-10"></i> Отправить
        </button>
        <div class="mt-30">
            <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('login-page')}}">
                <i class="fa fa-plus mr-5"></i> Вход
            </a>
            <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('user.register')}}">
                <i class="fa fa-plus mr-5"></i> Регистрация
            </a>
        </div>
    </div>
    @endif
</form>
<!-- END Sign In Form -->
@endsection
