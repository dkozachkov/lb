@extends('layouts.auth')

@section('content')
<!-- START Register In Form -->
<div id="v-registration">
    <form class="px-30" @submit.prevent="sendForm" data-vv-scope="registration" id='v-registration-form'>
        <h2 class="h5 font-w400 text-muted mb-20">Регистрация</h2>
        <div class="form-group row">
            <div class="col-12">
                <div class="form-material " :class="{'is-invalid': errors.has('registration.name')}">
                    <input type="text" class="form-control" class="field-modal-item" name="name" v-model="form.name"
                        v-validate="'required'">
                    <label for="login-username">ФИО</label>
                    <div class="invalid-feedback animated fadeInDown">
                        @{{ errors.first('registration.name') }}</div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="form-material " :class="{'is-invalid': errors.has('registration.email')}">
                    <input type="text" class="form-control" class="field-modal-item" name="email" v-model="form.email"
                        v-validate="'required|email'">
                    <label for="login-username">Ваш E-mail</label>
                    <div class="invalid-feedback animated fadeInDown">
                        @{{ errors.first('registration.email') }}</div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="form-material " :class="{'is-invalid': errors.has('registration.password')}">
                    <input type="password" class="form-control" autocomplete="off" v-model="form.password"
                        ref="password" v-validate="'required|min:8'" name="password">
                    <label for="login-password">Ваш пароль</label>
                    <div class="invalid-feedback animated fadeInDown">
                        @{{ errors.first('registration.password') }}</div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="form-material " :class="{'is-invalid': errors.has('registration.password_confirmation')}">
                    <input type="password" class="form-control" autocomplete="off" v-model="form.password_confirmation"
                        v-validate="'required|confirmed:password'" name="password_confirmation" data-vv-as="password">
                    <label for="login-password">Повторите пароль</label>
                    <div class="invalid-feedback animated fadeInDown">
                        @{{ errors.first('registration.password_confirmation') }}</div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-sm btn-hero btn-alt-primary " :class="{'loading': isLoading}"
                :disabled="isLoading">
                <i class="si si-login mr-10"></i> Отправить
            </button>
            <div class="mt-30">
                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('login-page')}}">
                    <i class="fa fa-plus mr-5"></i> Вход
                </a>
                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('password.request')}}">
                    <i class="fa fa-refresh mr-5"></i> Восстановить пароль
                </a>
            </div>
        </div>
    </form>
</div>
<!-- END Register In Form -->
@endsection

@push('scripts')
<script src="{{mix('js/pages/registration.js')}}"></script>
@endpush
