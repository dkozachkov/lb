<!-- Footer -->
<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        {{-- <div class="float-right">
            Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="https://stamax.com.ua"
                target="_blank">stamax</a>
        </div> --}}
        <div class="float-left">
            <a class="font-w600" href="https://leedbot.ru" target="_blank">Leedbot</a> &copy;
            <span class="js-year-copy"></span>
        </div>
    </div>
</footer>
<!-- END Footer -->
