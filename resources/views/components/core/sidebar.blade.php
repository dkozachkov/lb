<!-- Sidebar -->
<!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content sidebar-content-box">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow px-15">
            <!-- Mini Mode -->
            <div class="content-header-section sidebar-mini-visible-b">
                <!-- Logo -->
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                    <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                </span>
                <!-- END Logo -->
            </div>
            <!-- END Mini Mode -->

            <!-- Normal Mode -->
            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout"
                    data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->

                <!-- Logo -->
                <div class="content-header-item">
                    <a class="font-w700" href="/">
                        {{-- <i class="si si-fire text-primary"></i>--}}
                        <img src="{{ asset('images/default/logo.svg') }}" alt="leedbot" width="35">
                        <span class="font-size-xl text-dual-primary-dark">Leed</span><span
                            class="font-size-xl text-second">bot</span>
                        {{-- <span class="font-size-xl text-dual-primary-dark">code</span><span
                                                    class="font-size-xl text-primary">base</span> --}}

                    </a>
                </div>
                <!-- END Logo -->
            </div>
            <!-- END Normal Mode -->
        </div>
        <!-- END Side Header -->

        <!-- Side User -->
        <div class="content-side content-side-full content-side-user px-10 align-parent">
            <!-- Visible only in mini mode -->
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src="{{ asset('images/default/user.png') }}" alt="">
            </div>
            <!-- END Visible only in mini mode -->

            <!-- Visible only in normal mode -->
            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="javascript:void(0)">
                    <img class="img-avatar"
                        src="{{ auth()->user() && auth()->user()->avatar ? auth()->user()->avatar : asset('images/default/user.png') }}"
                        alt="">
                </a>

                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase"
                            href="javascript:void(0)">{{auth()->user()->name}}</a>
                        <a class="link-effect text-dual-primary-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                            <i class="si si-logout"></i>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    {{-- <li class="list-inline-item">
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <a class="link-effect text-dual-primary-dark" data-toggle="layout"
                            data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                            <i class="si si-drop"></i>
                        </a>
                    </li> --}}
                    {{-- <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="si si-logout"></i>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    </li> --}}
                </ul>
            </div>
            <!-- END Visible only in normal mode -->
        </div>
        <!-- END Side User -->
        <!-- Side Navigation -->
        <div class="content-side content-side-full sidebar-content__main-nav">
            <ul class="nav-main">
                <li class="{{isset($chatbot) ? 'open': ''}}">
                    <a class="{{ request()->is('/') ? ' active' : '' }} {{isset($chatbot) ? 'nav-submenu': ''}}"
                        href="/">
                        <i class="si si-diamond"></i><span class="sidebar-mini-hide">Проекты</span>
                    </a>

                    @isset($chatbot)
                    <ul>
                        <li class="nav-main-heading pt-10">
                            <span class="sidebar-mini-visible">Bot</span><span class="sidebar-mini-hidden text-primary">
                                {{$chatbot->title}}</span>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == 'chatbots.construction' ? ' active' : '' }}"
                                href="{{route('chatbots.construction', $chatbot->id)}}">Конструктор</a>
                        </li>
                        <li>
                            <a class="{{ in_array(Route::currentRouteName(), ['chatbots.page.show', 'chatbots.page.create']) ? ' active' : '' }}"
                                href="{{route($chatbot->has_page == 2 ? 'chatbots.page.show' : 'chatbots.page.create', $chatbot->id)}}">Стартовая
                                страница</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == 'chatbots.settings' ? ' active' : '' }}"
                                href="{{route('chatbots.settings', $chatbot->id)}}">Настройки</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == 'chatbots.integration' ? ' active' : '' }}"
                                href="{{route('chatbots.integration', $chatbot->id)}}">Установка</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == 'chatbots.integrations-local' ? ' active' : '' }}"
                                href="{{route('chatbots.integrations-local', $chatbot->id)}}">Интеграция чат-бота</a>
                        </li>
                    </ul>
                    @endisset

                </li>
                {{-- <li>
                    <a class="isDisabled {{ request()->is('home') ? ' active' : '' }}" href="/">
                <i class="si si-grid"></i><span class="sidebar-mini-hide">Мои шаблоны</span>
                </a>
                </li> --}}
                <li>
                    <a class="{{ request()->is('requests') ? ' active' : '' }}" href="/requests">
                        <i class="si si-list"></i><span class="sidebar-mini-hide">Заявки</span>
                    </a>
                </li>
                <li>
                    <a class="{{Route::currentRouteName() == 'integrations-global' ? ' active' : '' }}"
                        href="/integrations-global">
                        <i class="si si-settings"></i><span class="sidebar-mini-hide">Интеграции</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="content-side content-side-full">
            <ul class="nav-main">
                <li>
                    <a class="{{ Route::currentRouteName() == 'knowledge-base' ? ' active' : '' }}"
                        href="{{route('knowledge-base')}}">
                        <i class="si si-graduation"></i><span class="sidebar-mini-hide">База знаний</span>
                    </a>
                </li>
                <li>
                    <a class="{{ Route::currentRouteName() == 'help' ? ' active' : '' }}" href="{{route('help')}}">
                        <i class="si si-question"></i><span class="sidebar-mini-hide">Помощь</span>
                    </a>
                </li>
                <li>
                    <a class="{{ Route::currentRouteName() == 'suggest-idea' ? ' active' : '' }}"
                        href="{{route('suggest-idea')}}">
                        <i class="si si-plus"></i><span class="sidebar-mini-hide">Предложить идею</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- Sidebar Content -->
</nav>
<!-- END Sidebar -->
