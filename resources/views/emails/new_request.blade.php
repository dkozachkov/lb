<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>@lang('emails.new_request')</title>

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<?php
$style = [
    /* Layout ------------------------------ */
    'body' => 'margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;',
    'table' => 'border-collapse: collapse; width: 100%; margin-bottom: 1rem; color: #212529; border: 1px solid #dee2e6;',
    'thead' => 'display: table-header-group; vertical-align: middle; border-color: inherit; box-sizing: border-box;',
    'th' => 'display: table-cell; font-weight: bold; box-sizing: border-box; border: 1px solid #dee2e6; vertical-align: bottom; border-bottom: 2px solid #dee2e6; border-bottom-width: 2px;',
    'tr' => 'display: table-row; vertical-align: inherit; border-color: inherit; border: 1px solid #dee2e6;',
    'tbody' => 'display: table-row-group; vertical-align: middle; border-color: inherit; box-sizing: border-box;',
    'td' => 'display: table-cell; box-sizing: border-box; text-align: inherit; border-left: 1px solid #dee2e6; border-right: 1px solid #dee2e6; vertical-align: bottom; border-bottom: 2px solid #dee2e6; border-bottom-width: 2px;',
];
?>
<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; ?>
<body style="{{ $style['body'] }}">
<p>
    @lang('emails.new_request_text', ['chatbotTitle' => $chatbotTitle])
</p>

<table width="100%" cellpadding="0" cellspacing="0" style="{{ $style['table'] }}">
    <thead style="{{ $style['thead'] }}">
    <tr style="{{ $style['tr'] }}">
        <th style="{{ $style['th'] }}">Дата</th>
        <th style="{{ $style['th'] }}">Телефон</th>
        <th style="{{ $style['th'] }}">Имя</th>
        <th style="{{ $style['th'] }}">Email</th>
    </tr>
    </thead>
    <tbody style="{{ $style['tbody'] }}">
    <tr style="{{ $style['tr'] }}">
        <td style="{{ $style['td'] }}">{{ $chatbotRequest->created_at->format('d.m.Y H:i') }}</td>
        <td style="{{ $style['td'] }}">{{ $chatbotRequest->phone }}</td>
        <td style="{{ $style['td'] }}">{{ $chatbotRequest->name }}</td>
        <td style="{{ $style['td'] }}">{{ $chatbotRequest->email }}</td>
    </tr>
    </tbody>
</table>

<p>Лог вопросов-ответов:</p>

<table width="100%" cellpadding="0" cellspacing="0" style="{{ $style['table'] }}">
    <thead style="{{ $style['thead'] }}">
    <tr style="{{ $style['tr'] }}">
        <th style="{{ $style['th'] }}">Чатбот</th>
        <th style="{{ $style['th'] }}">Пользователь</th>
    </tr>
    </thead>
    <tbody style="{{ $style['tbody'] }}">
    @foreach($chatbotMessages as $message)
        <tr style="{{ $style['tr'] }}">
            @if($message->type !== 'answer')
                <td style="{{ $style['td'] }}">{{ $message->title }}</td>
                <td style="{{ $style['td'] }}"></td>
            @else
                @php
                    $title = '';

                    if (is_array($message->title)) {
                        $title = implode(', ', $message->title);
                    } else {
                        $title = $message->title;
                    }
                @endphp
                <td style="{{ $style['td'] }}"></td>
                <td style="{{ $style['td'] }}">{{ $title }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>

</body>
</html>
