@extends('layouts.clear')

@section('content')
<!-- Page Content -->
<div class="hero bg-white">
    <div class="hero-inner">
        <div class="content content-full">
            <div class="py-30 text-center">
                <div class="display-3 text-danger">
                    <i class="fa fa-warning"></i> 404
                </div>
                <h1 class="h2 font-w700 mt-30 mb-40">Страница не доступна</h1>
                <a class="btn btn-hero btn-rounded btn-alt-secondary" href="/">
                    <i class="fa fa-arrow-left mr-10"></i> На главную
                </a>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
