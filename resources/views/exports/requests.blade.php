<table>
    <thead>
    <tr>
        <th>№</th>
        <th>Дата</th>
        <th>Телефон</th>
        <th>Имя</th>
        <th>Email</th>
    </tr>
    </thead>
    <tbody>
    @foreach($requests as $request)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $request->created_at->format('d.m.Y H:i') }}</td>
            <td>{{ $request->phone }}</td>
            <td>{{ $request->name }}</td>
            <td>{{ $request->email }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
