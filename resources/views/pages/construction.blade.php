@extends('layouts.master')

@section('content')
<div id="v-constructor">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>
@endsection
@push('styles')
@endpush
@push('scripts')
<script>
    var chatbot = {!! $chatbot->toJson() !!};
</script>
<script src="{{mix('js/pages/constructor.js')}}"></script>
@endpush
