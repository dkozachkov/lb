@extends('layouts.clear')

@section('content')
<!-- Page Content -->
<div class="hero bg-white">
    <div class="hero-inner">
        <div class="content content-full">
            <div class="py-30 text-center">
                <div class="display-3 text-danger">
                    <i class="fa fa-warning"></i> 404
                </div>
                <h1 class="h2 font-w700 mt-30 mb-10">Чат-бот недоступен</h1>
                <h2 class="h3 font-w400 text-muted mb-50">Чат-бот отключен или удален</h2>
                <a class="btn btn-hero btn-rounded btn-alt-secondary" href="/">
                    <i class="fa fa-arrow-left mr-10"></i> К моим чат-ботам
                </a>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
