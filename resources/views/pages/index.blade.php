@extends('layouts.master')

@section('content')
<div id="v-main">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>
@endsection
@push('styles')
<link rel="stylesheet" href="{{mix('css/pages/index.css')}}">
@endpush
@push('scripts')
<script src="{{mix('js/pages/index.js')}}"></script>
@endpush
