@extends('layouts.master')

@section('content')
<div id="v-setup">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>
@endsection
@push('styles')
@endpush
@push('scripts')
<script>
    var chatbotId = {!! json_encode($data['id']) !!};
    var chatbotUUId = {!! json_encode($data['uuid']) !!};
    var pageData = {!! json_encode($data['page']) !!};
    var appData = {!! json_encode($data['integration']) !!};
</script>
<script src="{{mix('js/pages/setup.js')}}"></script>
@endpush
