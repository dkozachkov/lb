@extends('layouts.master')

@section('content')
<div id="v-integrations">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>
@endsection
@push('scripts')
<script>
    window.appData = {
        chatbotId: {{ isset($chatbot) ? $chatbot->id : 0 }}
    };
</script>
<script src="{{mix('js/pages/integrations.js')}}"></script>
@endpush
