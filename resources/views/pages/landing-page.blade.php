<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{$chatbot->page->seo->title}}</title>
        <meta name="description" content="{{$chatbot->page->seo->description}}" />
        <meta name="keywords" content="{{$chatbot->page->seo->keywords}}" />
        <link rel="icon" type="image/png" href="{{$chatbot->page->favicon}}">
        <link rel="stylesheet" href="{{ mix('css/pages/landing.css') }}">
    </head>

    <body>
        <header class="l-header" style="background-image: url({{$chatbot->page->background}})">
            <div class="l-header--bg"></div>
            <div class="container l-header__box">
                <a href="/" class="l-header__logo">
                    @if($chatbot->page->avatar)
                    <img src="{{$chatbot->page->avatar}}" alt="Leedbot">
                    @endif
                </a>
                <div class="l-header__title">
                    {!! $chatbot->page->title !!}
                </div>
                <div class="l-header__description">
                    {!! $chatbot->page->sub_title !!}
                </div>
            </div>
        </header>
        <main class="l-main">
            <div class="container">
                <div class="l-advantages">
                    @foreach ($chatbot->page->benefits as $item)
                    <div class="l-advantages__item">
                        <div class="l-advantages__item-head">
                            <div class="l-advantages__item-icon"
                                style="background-image: url({{ str_replace(' ', '%20', $item['image']) }})">
                            </div>
                            <div class="l-advantages__item-line"></div>
                        </div>
                        <div class="l-advantages__item-title">{{$item['text']}}</div>
                    </div>
                    @endforeach
                </div>

            </div>
            <div class="l-chatbot">
                <div class="container">
                    <div class="l-chat">
                        <div class="l-chat-page" id="v-leedbot_{{$chatbot->uuid}}">
                        </div>
                    </div>
                </div>
                <div class="l-footer">
                    @if($chatbot->page->seo->policy_url)
                    <a href="{{$chatbot->page->seo->policy_url}}" target="_blank">Политика конфиденциальности</a>
                    @endif
                </div>
            </div>
        </main>
        {{-- {{dd($chatbot->title)}} --}}

        <!-- LeedBot script start -->
        <script src="/widget/leedbot.js" type="application/javascript"></script>
        <script>
            document.addEventListener("DOMContentLoaded", function() {
                new LeedBot({
                    id: '{{$chatbot->uuid}}',
                    type: 'default',
                    selector: '#v-leedbot_{{$chatbot->uuid}}',
                    // type: 'button',
                    text: 'Чатбот',
                    color: '#fff',
                    background: '#4ACF34',
                });
            });
        </script>
        <!-- LeedBot script end -->
    </body>

    <script>
        window.pageData = {!! json_encode($chatbot) !!};
    </script>

</html>
