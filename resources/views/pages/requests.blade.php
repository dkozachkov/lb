@extends('layouts.master')

@section('content')
<div id="v-requests">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>
@endsection
@push('styles')
{{-- <link rel="stylesheet" href="{{mix('css/pages/settings.css')}}"> --}}
@endpush
@push('scripts')
<script>
    var chatbots = {!! $chatbots !!};
    var chatbotId = '{!! $chatbot_id !!}';
</script>
<script src="{{mix('js/pages/requests.js')}}"></script>
@endpush
