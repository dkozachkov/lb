@extends('layouts.master')

@section('content')
<div id="v-settings">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>
@endsection
@push('styles')
{{-- <link rel="stylesheet" href="{{mix('css/pages/settings.css')}}"> --}}
@endpush
@push('scripts')
<script>
    var settings = {!! $settings !!};
</script>
<script src="{{mix('js/pages/settings.js')}}"></script>
@endpush
