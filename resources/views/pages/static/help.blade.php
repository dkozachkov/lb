@extends('layouts.master')

@section('content')
<div id="v-help">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{mix('js/pages/help.js')}}"></script>
@endpush
