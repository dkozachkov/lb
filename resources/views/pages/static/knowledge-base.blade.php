@extends('layouts.master')

@section('content')
<div>
    <p><strong>Конструктор чат-бота</strong></p>
    <ol>
        <li><strong> Создание, редактирование, удаление блоков (элементов)</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Для создания блока нажмите на кнопку "Добавить блок". При таком создании связи
            между
            блоками не устанавливаются.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_1.png" width="415"
                height="212" /></span></p>
    <p><span style="font-weight: 400;">Для создания дополнительных блоков со связями кликните на значок "плюс" под
            имеющимся
            блоком.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_35.png" width="415"
                height="194" /></span></p>
    <p><span style="font-weight: 400;">Редактирование блоков осуществляется с помощью панели с правой стороны экрана,
            для ее
            открытия кликните на блок, который необходимо изменить и нажмите на кнопку "Редактировать", там же можно
            удалить
            блок, нажав на кнопку "Удалить".</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_2.png" alt="" width="415"
                height="199" /></span></p>
    <p><strong>!</strong><span style="font-weight: 400;">Редактирование блоков предполагает только изменение текста,
            если
            Вам необходимо включить множественный выбор, создать зависимость от ответа, то необходимо удалить блок и
            создать
            заново.</span></p>
    <p><span style="font-weight: 400;">Для перемещения блоков по рабочей области необходимо выбрать блок, зажав левую
            кнопку
            мыши, передвинуть его в необходимое место.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_15.png" alt=""
                width="300" height="210" /></span></p>
    <ol start="2">
        <li><strong> Типы сообщений</strong></li>
    </ol>
    <p><span style="font-weight: 400;">При создании блока у Вас есть возможность выбора типа сообщения:</span></p>
    <p><span style="font-weight: 400;"><img style="width: 400px;" src="/images/baza/Screenshot_3.png" alt="" width="400"
                height="221" /></span></p>
    <p><span style="font-weight: 400;">1) Информационный тип вопроса - текстовое сообщение пользователю</span></p>
    <table>
        <tbody>
            <tr>
                <td><img style="width: 285px;" src="/images/baza/Screenshot_4.png" alt="" width="285" height="162" />
                </td>
                <td><img style="width: 286px;" src="/images/baza/Screenshot_4_1.png" alt="" width="286" height="136" />
                </td>
            </tr>
        </tbody>
    </table>
    <p><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">2) Вопрос-ответ. Данный тип
            сообщения
            предполагает выбор ответа пользователем на заданный вопрос. При создании данного типа сообщения Вы можете
            добавлять любое количество ответов, нажимая на плюс.</span></p>
    <table>
        <tbody>
            <tr>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_5.png" alt="" width="300" height="340" />
                </td>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_5_2.png" alt="" width="300" height="336" />
                </td>
            </tr>
        </tbody>
    </table>
    <p><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">3) Свободный ввод предполагает вывод
            на
            экран пользователя поля, в котором можно осуществить ввод любого текстового сообщения.</span></p>
    <table>
        <tbody>
            <tr>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_6.png" width="300" height="204" /></td>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_6_2.png" alt="" width="300" height="230" />
                </td>
            </tr>
        </tbody>
    </table>
    <p><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">4) Дата. Данный тип сообщения
            позволяет
            выбрать необходимую дату пользователем на предложенном ему календаре.</span></p>
    <table>
        <tbody>
            <tr>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_7.png" alt="" width="300" height="205" />
                </td>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_7_2.png" alt="" width="300" height="236" />
                </td>
            </tr>
        </tbody>
    </table>
    <p><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">5) Время. Тип сообщения,
            предоставляющий
            пользователю выбор определенного времени.</span></p>
    <table>
        <tbody>
            <tr>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_8.png" width="300" height="202" />&nbsp;
                </td>
                <td><img style="width: 286px;" src="/images/baza/Screenshot_8_1.png" alt="" width="286"
                        height="143" />&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <p><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">6) Ползунок. Тип сообщения, которое
            предоставляет пользователю выбор числа путем перемещения ползунка слева направо. Минимальное и максимальное
            значение выбирается при создании вопроса.</span></p>
    <table>
        <tbody>
            <tr>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_9.png" alt="" width="300"
                        height="295" />&nbsp;</td>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_9_1.png" alt="" width="300" height="331" />
                </td>
            </tr>
        </tbody>
    </table>
    <p><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">7) Выбор списка (дроплист).
            Предполагает
            выбор конкретного ответа пользователем из составленного списка.</span></p>
    <table>
        <tbody>
            <tr>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_10.png" alt="" width="300"
                        height="323" />&nbsp;</td>
                <td><img style="width: 300px;" src="/images/baza/Screenshot_10_1.png" alt="" width="300" height="314" />
                </td>
            </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>
    <ol start="3">
        <li><strong> Множественный выбор, любой ответ, обязательное поле</strong></li>
    </ol>
    <p><span style="font-weight: 400;">1) Множественный выбор предполагает возможность выбора пользователем нескольких
            ответов на один вопрос. Чтобы активировать множественный выбор. поставьте напротив него галочку.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_11.png" alt=""
                width="300" height="222" /></span></p>
    <p><span style="font-weight: 400;">2) Любой ответ. Для создания зависимости следующего сообщения чат-бота от ответа
            пользователя, необходимо убрать соответствующую галочку.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_12.png" alt=""
                width="300" height="228" /></span></p>
    <p><strong>!</strong><span style="font-weight: 400;">Настроить зависимость будет возможно только после создания
            нового
            блока. Вам высветится окно, где есть возможность настроить условие.&nbsp;</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_13.png" alt=""
                width="300" height="189" /></span></p>
    <p><span style="font-weight: 400;">При добавлении связи к блоку для установки зависимости, Вам также откроется окно
            на
            добавление необходимого условия.</span></p>
    <p><span style="font-weight: 400;">При выборе типов вопросов: свободный ввод, ползунок, дата, время, дроплист,
            создавая
            зависимость от ответа, Вам будет необходимо в появившемся окне "Ответы" написать варианты ответов, которые в
            дальнейшем будут использоваться в условии при создании зависимости.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_14.png" alt=""
                width="300" height="323" /></span></p>
    <p><span style="font-weight: 400;">3) Обязательное поле. При отключении галочки напротив, у пользователя появляется
            возможность не отвечать на заданный вопрос.&nbsp;</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_16.png" alt=""
                width="300" height="226" /></span></p>
    <ol start="4">
        <li><strong> Добавление, удаление связей</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Чтобы добавить связь между блоками необходимо кликнуть на кнопку "Добавить
            связь",
            выбрать блок, от которого связь будет начинаться, далее выбрать еще один блок - окончание связи.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_17.png" width="415"
                height="170" /></span></p>
    <p><strong>!</strong><span style="font-weight: 400;">Блоки выбираются кликом мыши.</span></p>
    <p><span style="font-weight: 400;">Для редактирования или удаления связи между блоками необходимо нажать на
            соединяющую
            блоки стрелку и в правом окне выполнить необходимые действия.</span></p>
    <p><img style="width: 415px;" src="/images/baza/Screenshot_18.png" alt="" width="415" height="174" /><br /><br />
    </p>
    <p><strong>Стартовая страница</strong></p>
    <ol>
        <li><strong> Графический контент страницы</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Для добавления/удаления фонового изображения, логотипа проекта или favicon
            страницы,
            необходимо навести мышкой на изображение и выбрать необходимое действие. Для просмотра страницы, кликните по
            кнопке "Просмотр" в правой верхней части экрана.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_19.png" width="415"
                height="204" /></span></p>
    <p><span style="font-weight: 400;">Рекомендуем использовать фоновое горизонтальное изображение с соотношением сторон
            16:9 и с наименьшим весом для быстрой загрузки сайта. Поддерживаемые расширения фонового изображения: jpeg,
            jpg,
            png. Расширение для логотипа проекта: png (с прозрачным фоном). Рекомендуемое расширение для иконки страницы
            (favicon): ico. Рекомендуемые размеры 16х16, 32х32 и 48х48.</span></p>
    <ol start="2">
        <li><strong> Текстовый контент страницы.</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Включает в себя заголовок, подзаголовок и 3 преимущества, значки которых
            выбираются
            из библиотеки. Количество символов в заголовке и подзаголовке страницы не может превышать 255 символов. Для
            написания огненного заголовка рекомендуем ознакомится с технологией 4U.&nbsp;</span></p>
    <ol start="3">
        <li><strong> Дополнительный настройки: seo блок + основные настройки</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Данный блок необходим на сайте для целей продвижения в поисковых системах. Title
            &ndash; это заголовок html-документа, используется в результатах поисковых систем. Description и Keywords -
            мета-теги, содержат в себе ключевые слова и описание страницы. Рекомендуемая длина Title не более 70-80
            символов, Description - не более 150-160 символов.&nbsp;Рекомендуем использовать на сайте ссылку на политику
            конфиденциальности, так как по закону владельцы сайтов имеют право обрабатывать персональные данные клиентов
            только после их согласия.</span></p>
    <p>&nbsp;</p>
    <p><strong>Настройка чат-бота</strong></p>
    <ol>
        <li><strong> Сбор контактов</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Сбор контактов от пользователя осуществляется в конце диалога.&nbsp;Синий
            переключатель с правой стороны страницы позволяет включать/выключать необходимые поля сбора
            контактов.</span>
    </p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_20.png" alt=""
                width="415" height="167" /></span></p>
    <p><span style="font-weight: 400;">Текст подсказки - текст, который будет виден пользователю, например "Введите
            номер
            телефона", "Введите имя" и т. д. Маска телефона облегчает ввод данных для пользователя. Пример маски:
            79#########.</span></p>
    <ol start="2">
        <li><strong> Форма благодарности</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Форма, которую увидит пользователь после взаимодействия с формой захвата. С
            помощью
            настроек Вы сможете создать индивидуальное сообщение пользователю.&nbsp;&nbsp;</span></p>
    <p><span style="font-weight: 400;"><img style="width: 400px;" src="/images/baza/Screenshot_21.png" width="400"
                height="201" /></span></p>
    <p><span style="font-weight: 400;">Используйте поле переадресации пользователя после ответа для направления
            пользователя
            на любую другую страницу: основной сайт, группа в социальных сетях и т.д. Формат этого поля -
            ссылка.&nbsp;</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_22.png" alt=""
                width="415" height="167" /></span></p>
    <ol start="3">
        <li><strong> Дизайн чат-бота</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Аватар бота - лицо Вашего бота, которое будет видно пользователю.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_23.png" alt=""
                width="300" height="241" /></span></p>
    <p><span style="font-weight: 400;">Фон виджета - это задний фон, на котором происходит диалог, Вы можете выбрать
            изображения для фона, либо воспользоваться настройками. Также Вы можете полностью стилизовать свой чат-бот в
            настройках внизу страницы.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_25.png" width="415"
                height="214" /></span></p>
    <p><strong>Установка чат-бота</strong></p>
    <ol>
        <li><strong> Установка на готовый сайт</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Если у Вас есть уже созданный сайт, Вы можете встроить в него чат-бота так, чтобы
            он
            показывался отдельным блоком на сайте.&nbsp;</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_26.png" alt=""
                width="415" height="160" /></span></p>
    <p><span style="font-weight: 400;">Для этого введите укажите доменное имя сайта, нажмите на кнопку "Сохранить" и в
            окне
            ниже сгенерируется специальный код, который будет необходимо вставить&nbsp;в блок "head" на Вашем сайте.
            Чтобы
            скопировать данный код - нажмите на кнопку "Копировать" внизу страницы.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_27.png" width="415"
                height="223" /></span></p>
    <p><span style="font-weight: 400;">Также не забудьте вставить селектор чат-бота в id элемента на странице сайта где
            он
            должен отображаться.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 361px;" src="/images/baza/Screenshot_31.png" alt=""
                width="361" height="52" /></span></p>
    <ol start="2">
        <li><strong> Кнопка</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Данная установка предполагает установку чат-бота на сайт в виде кнопки, нажав на
            которую будет открываться окно с текстовыми сообщениями чат-бота. Местоположение кнопки, Вы можете
            устанавливать
            сами, путем редактирования HTML кода. Установка чат-бота производится также, как при установке на готовый
            сайт.
            У Вас есть возможность выбрать цвет фона и текста для кнопки и посмотреть пример, как это будет
            выглядеть.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 415px;" src="/images/baza/Screenshot_28.png" alt=""
                width="415" height="160" /></span></p>
    <ol start="3">
        <li><strong> Прямая ссылка на страницу</strong></li>
    </ol>
    <p><span style="font-weight: 400;">Если у вас нет сайта или Вы хотите, чтобы чат-бот показывался на новом сайте, Вы
            можете создать его самостоятельно на нашем сервисе. Настройка сайта происходит в разделе "Стартовая
            страница" с
            левой стороны экрана. В нее также можно перейти через кнопку "Настройка страницы".&nbsp;</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_29.png" alt=""
                width="300" height="87" /></span></p>
    <p><span style="font-weight: 400;">Пример страницы, созданной на нашем сервисе, представлен ниже.</span></p>
    <p><span style="font-weight: 400;"><img style="width: 300px;" src="/images/baza/Screenshot_30.png" alt=""
                width="300" height="286" /></span></p>
</div>
@endsection
