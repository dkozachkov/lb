@extends('layouts.master')

@section('content')
<div class="text-center">
    <h1>Leedbot</h1>
    <h4>
        Скажите, что нам нужно улучшить в нашем проекте?
    </h4>
    <p>
        Просто напишите нам на почту свое предложение с темой письма «Leedbot - идея»

    </p>
    <p>
        Почта для идей:
        <a href="mailto:support@leedbot.ru">support@leedbot.ru</a>

    </p>

</div>
@endsection
