@extends('layouts.master')

@section('content')
<div id="v-template">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>
@endsection
@push('styles')
<link rel="stylesheet" href="{{mix('css/pages/template.css')}}">
@endpush
@push('scripts')
<script>
    var chatbotId = {!! $chatbot_id !!};
    var chatbot = {!! $chatbot->toJson() !!};
    var page = {!! $page ?? "null" !!};
</script>
<script src="{{mix('js/pages/template.js')}}"></script>
@endpush
