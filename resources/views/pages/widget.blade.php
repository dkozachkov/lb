<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>LeedBot-widget</title>
        <style>
            html,
            body {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
            }
        </style>
    </head>

    <body>
        <div id="app"></div>
        <!-- LeedBot script start -->
        <script src="/widget/init.js" type="application/javascript"></script>
        <!-- LeedBot script end -->
    </body>

</html>
