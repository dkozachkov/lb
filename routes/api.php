<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Auth\RegisterController@register')->name('register');

Route::group(['middleware' => ['cors'], 'namespace' => 'Api', 'prefix' => 'chatbot/{uuid}'], function () {
    Route::get('/', 'CorsController@get');

    Route::post('/', 'CorsController@save');
});

Route::group(['middleware' => ['auth:api'], 'namespace' => 'Api'], function () {

    Route::group(['prefix' => 'icons', 'as' => 'icons'], function () {
        Route::get('categories', 'IconController@getCategories')->name('get_categories');
        Route::get('{categoryId}', 'IconController@getIcons')->where(['categoryId' => '[0-9]+'])->name('get_icons');
    });

    Route::group(['prefix' => 'chatbots/{chatbotId}', 'as' => 'chatbots.'], function () {

        Route::patch('change/title', 'ChatbotController@changeTitle')->name('change.title');

        Route::group(['prefix' => 'requests', 'as' => 'requests.'], function () {
            Route::get('/export', 'ChatbotRequestController@export')->name('export');
            Route::patch('/{requestId}', 'ChatbotRequestController@toggleStatus')->name('toggle-status');
            Route::delete('/{requestId}', 'ChatbotRequestController@delete')->name('destroy');
            Route::get('/', 'ChatbotRequestController@index')->name('index');
        });

        Route::get('integration', 'ChatbotIntegrationController@show')->name('integration.show');
        Route::put('integration', 'ChatbotIntegrationController@update')->name('integration');

        Route::get('construction', 'ChatbotConstructionController@get')->name('construction.get');
        Route::put('construction', 'ChatbotConstructionController@update')->name('construction');

        Route::patch('toggle-status', 'ChatbotController@toggleStatus')->name('status.toggle');

        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
            Route::get('/reset', 'ChatbotSettingController@reset')->name('reset');
            Route::put('/', 'ChatbotSettingController@update')->name('update');
            Route::get('/', 'ChatbotSettingController@show')->name('show');
        });

        Route::group(['prefix' => 'page', 'as' => 'page.'], function () {
            Route::get('/', 'ChatbotPageController@show')->name('show');
            Route::put('/update', 'ChatbotPageController@update')->name('update');
            Route::post('/', 'ChatbotPageController@store')->name('store');
        });

        Route::post('duplicate', 'ChatbotController@duplicate')->name('duplicate');

    });

    Route::group(['prefix' => 'integrations', 'as' => 'integrations'], function () {
        Route::group(['prefix' => 'emails/{chatbotId?}', 'as' => 'emails'], function () {
            Route::get('/', 'ServiceIntegrationEmailController@index')->name('index');
            Route::put('/', 'ServiceIntegrationEmailController@update')->name('update');
            Route::patch('/', 'ServiceIntegrationEmailController@changeStatus')->name('changeStatus');
        });
    });

    Route::apiResource('chatbots', 'ChatbotController');
});
