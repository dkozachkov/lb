<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('widgets', function () {
    return view('pages.widget');
})->name('widget');

Route::post('/register', 'Auth\AuthController@register')->name('register');
Route::post('/login', 'Auth\AuthController@login')->name('login');
Route::view('/login', 'auth.login')->name('login-page');

Route::group(['namespace' => 'Auth'], function () {
    Route::get('/register', 'RegisterController@showRegistrationForm')->name('user.register');

    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');

    Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
    Route::get('/callback/{provider}', 'SocialController@callback');
});

Route::group(['middleware' => 'not_guest'], function () {
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::group(['prefix' => 'requests', 'as' => 'requests.'], function () {
        Route::get('/', 'ChatbotRequestController@index')->name('index');
        Route::get('/{requestId}', 'ChatbotRequestController@show')->name('show');
    });
    Route::group(['prefix' => 'chatbots/{chatbotId}', 'as' => 'chatbots.'], function () {
        Route::get('construction', 'ChatbotConstructionController')->name('construction');
        Route::get('integration', 'ChatbotIntegrationController')->name('integration');
        Route::get('settings', 'ChatbotSettingController')->name('settings');
        Route::get('integrations-local', 'ChatbotServiceIntegrationController')->name('integrations-local');

        Route::group(['prefix' => 'page', 'as' => 'page.'], function () {
            Route::get('create', 'ChatbotPageController@create')->name('create');
            Route::get('show', 'ChatbotPageController@show')->name('show');
        });
    });

    Route::group(['prefix' => 'integrations', 'as' => 'integrations'], function () {
        Route::get('emails/{chatbotId?}', 'ServiceIntegrationEmailController@index');
    });

    Route::view('/', 'pages.index')->name('home');
    Route::view('/knowledge-base', 'pages.static.knowledge-base')->name('knowledge-base');
    Route::view('/suggest-idea', 'pages.static.suggest-idea')->name('suggest-idea');
    Route::view('/help', 'pages.static.help')->name('help');
    Route::view('/integrations-global', 'pages.integrations')->name('integrations-global');
});


Route::get('/page/{chatbotUuid}', 'PageController')->name('page');

//\Auth::routes();
