const mix = require('laravel-mix');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const fs = require('fs');

// Расширеный конфиг для вебпака
mix.webpackConfig({
    optimization: {
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            // maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 8,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            vue$: 'vue/dist/vue.js',
            '~': __dirname + '/',
            '@': __dirname + '/resources/'
        }
    },
    plugins: [
        // new BundleAnalyzerPlugin(),
    ]
});

// Хелпер для работы с сингл файлами
let getFiles = function(dir) {
    return fs.readdirSync(dir).filter(file => {
        return fs.statSync(`${dir}/${file}`).isFile();
    });
};

//Хотрелоуд при изменении файлов
mix.browserSync({
    open: 'external',
    host: process.env.APP_LOCAL,
    proxy: process.env.APP_LOCAL,
    files: ['resources/views/**/*.php', 'app/**/*.php', 'routes/**/*.php', 'public/']
});

// Single file for client start
getFiles('resources/js/pages').forEach(function(filepath) {
    mix.js('resources/js/pages/' + filepath, 'public/js/pages/');
});
getFiles('resources/sass/pages').forEach(function(filepath) {
    mix.sass('resources/sass/pages/' + filepath, 'public/css/pages/');
});
// Single file for client end

mix.js('resources/js/app.js', 'js/app.js')
    .autoload({
        jquery: ['$', 'jQuery', 'window.jQuery', 'window.$'],
        'popper.js/dist/umd/popper.js': ['Popper']
    })
    .sass('resources/sass/main.scss', 'public/css')
    .copyDirectory('resources/images', 'public/images');
//    .copyDirectory('resources/fonts', 'public/fonts')
mix.js('resources/js/widget/index.js', 'widget/leedbot.js');
mix.js('resources/js/widget/init.js', 'widget/init.js');

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
